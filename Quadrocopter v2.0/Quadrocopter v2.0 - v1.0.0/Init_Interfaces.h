/*
 * Init_Interfaces.h
 */ 

#ifndef INIT_INTERFACES_H_
#define INIT_INTERFACES_H_

#include <stdio.h>											// funkce pro vstup a v�stup (s�riov� linka)...
#include "Init_Xmega.h"										// inicializace Xmegy
#include "Konfigurace.h"									// konfigurace pro r�zn� typy desek
#include "twi_master_driver.h"								// I2C sb�rnice

#define CS_AG(X)		PORTE.OUT ## X = PIN4_bm			// CS pin SPI pro AG (SET - komunikace off, CLR - komunikace on)
#define CS_M(X)			PORTF.OUT ## X = PIN4_bm			// CS pin SPI pro M (SET - komunikace off, CLR - komunikace on)
#define SPIPort			PORTE
#define SPICTRL			SPIE
#define SPIMO			7
#define SPIMI			6
#define SPISC			5
#define SPISS			4									// pro SPI

void Spiopen();										// SPI open
void Spiclose();									// SPI close
int SpiReadWrite(char val);							// SPI read/write
void CS_SPI(char addr);								// SPI adresa (PE.4 pro CS_AG, PF.4 pro CS_M)
void setUpSerial();									// nastaven� UARTu
int uart_putchar (char c, FILE *stream);			// odes�l�n� znak� printf na UART
int uart_getchar(FILE *stream);						// �ten� znak� scanf z UARTu
void remap();										// P�emapov�n� UARTu na port C
void initI2C();										// nastaven� I2C
void I2C_write(char addr, char addr_reg, char value_reg);		// �ten� dat z LPS331AP
void I2C_read(char addr, char reg);					// adresov�n� I2C za��zen� pro �ten�

#endif /* INIT_INTERFACES_H_ */