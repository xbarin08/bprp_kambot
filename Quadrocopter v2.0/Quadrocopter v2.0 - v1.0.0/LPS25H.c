/*
 * LPS25H.c
 */ 

#include "LPS25H.h"										// jeho hlavi�ka

float data = 0;										// meziprom�nn� pro v�stupn� data

char I2C_Who_Am_I(char addr, int reg, int WhoAmI)		// I2C testov�n� WhoAmI registr� + vyhodnocen� err
{
	char data;											// pomocn� prom�nn� pro data z SPI
	
	I2C_read(addr, reg);								// vol� fci pro adresaci �ten� z I2C, kter� p�ed�v� adresu a registr I2C za��zen�
	
	data = TWIC.MASTER.DATA;											// spodn�ch 8b meziprom zaplnit t�et�m p�ijat�m B
	TWIC.MASTER.CTRLC = TWI_MASTER_ACKACT_bm | TWI_MASTER_CMD_STOP_gc;	// odeslat NAK, ukon�it komunikaci
	
	if (data != WhoAmI)									// souhlas� registr s datasheetem?
	{
		return WhoAmI;									// vr�tit p�ijat� data, pokud reg. nesouhlas� s datasheetem
	}
	
	return(0);											// v�e prob�hlo OK
}

void Init_LPS25H()										//  konfigurace barometru LPS25H
{
	err[LPS25Herr] = I2C_Who_Am_I(LPS25H, LPS25H_WHO_AM_I, 0xBD);		// fce. testov�n� LPS25H (chyba = 0xBD - spr�vn� hodnota ID pro testov�n�)

	I2C_write(LPS25H, LPS25H_CTRL_REG1, 0x84);//C4
	I2C_write(LPS25H, LPS25H_RES_CONF, 0x00);//0F
	I2C_write(LPS25H, LPS25H_CTRL_REG2, 0x01);//60
	I2C_write(LPS25H, LPS25H_FIFO_CTRL, 0x00);//C0//DF
	//I2C_write(LPS25H, LPS25H_CTRL_REG2, 0x40);
	//I2C_write(LPS25H, LPS25H_FIFO_CTRL, 0xDF);							// nov� konfigurace pro LPS25H
}

float I2C_read_multi(char addr, int reg, int bytu)		// �ten� v�ce bajt� na I2C
{
	uint8_t rawData[3] = {0, 0, 0};						// do�asn� prom�nn� pro p�ijat� bajty (maxim�ln� t�i, v�t�ina senzor� m� dva a� t�i, tak�e zbyte�n� neo�et�uju)
	//float data = 0;										// meziprom�nn� pro v�stupn� data
	uint8_t i = 0;										// pomocn� prom�nn� pro for cyklus
	
	I2C_read(addr, reg);								// vol� fci pro adresaci �ten� z I2C, kter� p�ed�v� adresu a registr I2C za��zen�

	for(i = 1; i < bytu ; ++i)							// cyklus pro p��jem v�ce jak 1 bajtu
	{
		rawData[i-1] = TWIC.MASTER.DATA;				// pln� postupn� pole p�ijat�mi bajty
		
		TWIC.MASTER.CTRLC = TWI_MASTER_CMD_RECVTRANS_gc;				// po prvn�m B odeslat ACK
		while(!(TWIC_MASTER_STATUS & TWI_MASTER_RIF_bm));				// �ek�n� na odesl�n�
	}

	rawData[i-1] = TWIC.MASTER.DATA;									// posledn� p�ijat� bajt do pole
	TWIC.MASTER.CTRLC = TWI_MASTER_ACKACT_bm | TWI_MASTER_CMD_STOP_gc;	// odeslat NAK, ukon�it komunikaci
	
	data = ((uint32_t)rawData[2] << 16) | ((uint16_t)rawData[1] << 8) | (uint8_t)rawData[0];
	
	//-----------------------------------------------
	
	data = (data/4096.0) + 28.4;										// p�epo�et meziprom na tlak v hPa (mbar) + offset vznikl� p�i p�jen� = 28.4 hPa
//printf("ahoj\n");
	//float vyska = 0;
	//extern float temperature;
	//extern uint32_t Now;
	//temperature = 20;
	//vyska = ((powf((1013.25f / data), 0.1902630958f) - 1.0f) * (temperature + 273.15f)) / 0.0065f;
	//vyska = (18464 * (1 + (0.003663 * temperature)) * (log10(1032.1777 / data)));
	//printf("%d%d%d\n", rawData[0], rawData[1], rawData[2]);
	//printf("%.6f\n", data);
	//printf("%.2f\n", temperature);
	//printf("%.2f\n", vyska);
	
	//printf("%.3f\n", data);
	
	return(0);										// v�e prob�hlo OK
}