/*
 * Konfigurace.h
										Konfigurace pro r�zn� typy (n�vrhy/revize) desek
 */ 


#ifndef KONFIGURACE_H_
#define KONFIGURACE_H_

#define INFO_TISK		printf("\t *****Projekt: Quadrocopter v2.0  |  Program: v2.0*****\n")	// tisk informac�
#define VILIK_DPS_PC7_ERR								// ur�uje, zda je pou�ita deska s vadn�ch UARTem na C1 -> sm�ruje na USART C0

uint8_t err[4];											// pole 3 prvk� pro errory, err0 = err, err1 = WDT, err2 = SPI, (0 = v�e OK)

enum err												// enum pro pole v�ech error� (prvek 0 ud�v� celkov� po�et zaznamenan�ch error�)
{
	errors,												// celkem
	WDTerr,												// watchdog timer
	SPIerr,												// SPI - LSM9DS1...pokud nedetekuje ACC/GYRO nebo MAG, tak vyp�e jeho who am i hodnotu, p��padn� sou�et obou, pokud nenav�e komunikaci ani s jedn�m
	LPS25Herr,											// chyba v komunikaci s LPS25H (barometr)...vr�t� who am i
};

#endif /* KONFIGURACE_H_ */