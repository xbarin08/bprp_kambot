/*
 * Fusion_filter.h
 */ 
#ifndef INIT_FUSION_FILTER_H_
#define INIT_FUSION_FILTER_H_

#include <math.h>									// matematick� knihovna (odmocniny,...)

#define		Kp			10.0f*0.5f					// proporcion�ln� zes�len� f�zn�ho filtru rotace (Mahony)
#define		Ki			0.0f*0.5f					// integr�ln� zes�len� f�zn�ho filtru rotace (Mahony)
#define		BETA		0.15f						// proporcion�ln� zes�len� f�zn�ho filtru rotace (Madgwick)

void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz, float deltat);
void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float deltat);

void MahonyAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz, float deltat);
void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float deltat);

float invSqrt(float x);

#endif /* INIT_FUSION_FILTER_H_ */