/*
 * Basic_Interrupts.h
 */ 


#ifndef BASIC_INTERRUPTS_H_
#define BASIC_INTERRUPTS_H_

#include "Init_Interfaces.h"						// inicializace Xmegy

ISR(TCC0_OVF_vect);									// funkce přerušení pro TCC0
ISR(PORTF_INT0_vect);								// funkce pro obsluhu zapínacího tlačítka DPS (EXT. INT0 na PF)
ISR(PORTB_INT0_vect);								// Fce pro obsluhu RC ovladače(EXT. INT0 na PB)
//ISR(USARTC0_RXC_vect);

#endif /* BASIC_INTERRUPTS_H_ */