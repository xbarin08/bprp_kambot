/*
 * LSM9DS1.c
 */ 

#include "LSM9DS1.h"								// jeho hlavi�ka

extern float q[4];									// v�choz� vektor kvaternionu	(glob�ln� - udr�uje posledn� vypo�tenou hodnotu)
uint32_t lastUpdate = 0;							// okam�ik posledn�ho v�po�tu integra�n�ho intervalu deltat (glob�ln� - udr�uje posledn� dobu)
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0},  magBias[3] = {0, 0, 0};	// Offset gyro, acc, mag (glob�ln� - udr�uje offset stanoven� kalibrac� senzoru)
float aRes, gRes, mRes;								// prom�nn� pro ulo�en� nastaven�ho rozli�en� senzor�
extern float ax, ay, az, gx, gy, gz, mx, my, mz;	// prom�nn� v�stupn�ch hodnot acc, gyra, mag (jednotky: g, �/s, G)
float gx_reg, gy_reg;
float axp, ayp, azp, mxp, myp, mzp;					// prom�nn� pro p�epo�et zrychlen� z g -> m/(s^2) a z G -> uT
float temperature;									// p�epo�et tempCount na �C -> teplota senzoru LSM9DS1
float R [3][3];
unsigned int filtr = 1;
uint32_t pomoc_t = 0;
float pitch, yaw, roll;								// prom�nn� pro vypo�ten� Eulerovy �hly
uint8_t smycka = 0; // pomocn� prom. pro tisk //************

float linAcc[3] = {0.0f, 0.0f, 0.0f};

// prom�nn� pro nastaven� LSM9DS1
uint8_t Gscale = GFS_500DPS;						// gyro rozsah, rozli�en�
uint8_t Godr = GODR_476Hz;							// gyro vzorkovac� kmito�et
uint8_t Gbw = GBW_highest;							// gyro data bandwidth
uint8_t Ascale = AFS_4G;							// acc rozsah, rozli�en�
uint8_t Aodr = AODR_476Hz;							// acc vzorkovac� kmito�et
uint8_t Abw = ABW_408Hz;							// acc data bandwidth
uint8_t Mscale = MFS_4G;							// mag rozsah, rozli�en�
uint8_t Modr = MODR_20Hz;							// mag vzorkovac� kmito�et
uint8_t Mmode = MMode_UltraHighPerformance;			// mag operation mode

char initSPI(char addr, int reg, int WhoAmI)		// SPI testov�n� WhoAmI registr� + vyhodnocen� err
{
	char data;											// pomocn� prom�nn� pro data z SPI
	
	CS_SPI(addr);										// povolen� komunikace s SPI za��zen�m
	SpiReadWrite(reg |= READ);							// ��dost o �ten� registru z SPI
	data = SpiReadWrite(0xFF);							// �ten� registru z SPI
	CS_SPI(addr);										// zak�z�n� komunikace s SPI za��zen�m
	
	if (data != WhoAmI)									// souhlas� registr s datasheetem?
	{
		return WhoAmI;									// vr�tit p�ijat� data, pokud reg. nesouhlas� s datasheetem
	}
	
	return(0);											// v�e prob�hlo OK
}

void writeByte(uint8_t addr, int reg, uint8_t data)	// zap�e byte na danou adresu registru
{
	CS_SPI(addr);										// povolen� komunikace s SPI za��zen�m
	SpiReadWrite(reg);									// ��dost o �ten� registru z SPI
	SpiReadWrite(data);									// z�pis do registru
	CS_SPI(addr);										// ukon�en� komunikace s dan�m za��zen�m
}

uint8_t readByte(uint8_t addr, int reg)				// p�e�te byte na dan� adrese registru
{
	uint8_t data = 0;									// do�asn� prom�nn� pro data
	
	CS_SPI(addr);										// povolen� komunikace s SPI za��zen�m
	SpiReadWrite(reg |= READ);							// ��dost o �ten� registru z SPI
	data = SpiReadWrite(0xFF);							// �ten� registru z SPI
	CS_SPI(addr);										// zak�z�n� komunikace s SPI za��zen�m
	return (data);
}

void readBytes(uint8_t addr, int reg, uint8_t num, uint8_t *pole_dat)		// p�e�te num-byt� na dan� adrese registru a zap�e je do pole_dat
{
	uint8_t i = 0;										// pomocn� prom�nn�
	
	CS_SPI(addr);										// adresa za��zen�
	Spiopen();											// povolen� SPI
	SPICTRL.DATA = (reg |= READ);						// �ten� na adrese registru
	while(!(SPICTRL.STATUS & (1<<7)));					// �ek�n� na odesl�n�
	
	for (i = 0; i < num; ++i)							// vy�ten� num-byt�
	{
		SPICTRL.DATA = (0xFF);							// neru�it (nezapisuju, pos�l�m "nic", proto�e �ek�m na jeho data)
		while(!(SPICTRL.STATUS & (1<<7)));				// p��jem bytu
		pole_dat[i] = SPICTRL.DATA;						// z�pis bytu do "i" prvku pole_dat, resp. do pole, na kter� ukazuje pole_dat
	}
	Spiclose();											// povolen� komunikace s SPI za��zen�m
	CS_SPI(addr);										// zak�z�n� komunikace s SPI za��zen�m
	}

void InitLSM9DS1()									// nastaven� registr� akcelerometru, gyra a magnetometru na SPI
{
	err[SPIerr] = initSPI(AG, LSM9DS1XG_WHO_AM_I, 0x68);			// fce. testov�n� AG (chyba = 0x68 - spr�vn� hodnota ID pro testov�n�)
	err[SPIerr] += initSPI(MAG, LSM9DS1M_WHO_AM_I, 0x3D);			// fce. testov�n� M (chyba = 0x3D - spr�vn� hodnota ID pro testov�n�, pokud je chyba i u AG, potom err[SPIerr] = 0x68 + 0x3D)
	
	if (err[SPIerr] == 0)
	{
		getAres();
		getGres();
		getMres();													// fce pro p�epo�et rozli�en� podle nastaven�ch prom�nn�ch

		printf("Spu�t�n� self-testu akcelerometru a gyroskopu...\n\n");
		selftestLSM9DS1();											// self-test akcelerometru a gyra
		
		printf("Spu�t�n� kalibrace akcelerometru a gyroskopu...\n");
		accelgyrocalLSM9DS1(gyroBias, accelBias, 5000);				// kalibrace akcelerometru a gyra, load biases in bias registers
		
		printf("\t accel biases x|y|z (mg): %f | %f | %f\n", (1000.*accelBias[0]), (1000.*accelBias[1]), (1000.*accelBias[2]));
		printf("\t gyro biases x|y|z (dps): %f | %f | %f\n\n", (1000.*gyroBias[0]), (1000.*gyroBias[1]), (1000.*gyroBias[2]));
		
		printf("Spu�t�n� kalibrace magnetometru...");
		//magcalLSM9DS1(magBias);										// kalibrace magnetometru, load biases in bias registers
		_delay_ms(10);												// z�hadn� prodleva, bez kter� nefunguje n�sleduj�c� printf
		printf("\t mag biases x|y|z (mG): %f | %f | %f\n\n", (1000.*magBias[0]), (1000.*magBias[1]), (1000.*magBias[2]));

		// ACC/GYRO - nastaven�:
		writeByte(AG, LSM9DS1XG_CTRL_REG4, 0x38);										// povolen� 3-axes gyro
		writeByte(AG, LSM9DS1XG_CTRL_REG1_G, Godr << 5 | Gscale << 3 | Gbw);			// konfigurace gyra
		writeByte(AG, LSM9DS1XG_CTRL_REG5_XL, 0x38);									// povolen� 3-axes akcelerometru
		writeByte(AG, LSM9DS1XG_CTRL_REG6_XL, Aodr << 5 | Ascale << 3 | 0x04 |Abw);		// konfigurace "accelerometer-specify bandwidth selection with Abw"
		writeByte(AG, LSM9DS1XG_CTRL_REG8, 0x44);										// povolen� "block data update, allow auto-increment during multiple byte read"
		// MAGNETOMETR - nastaven�:
		writeByte(MAG, LSM9DS1M_CTRL_REG1_M, 0x80 | Mmode << 5 | Modr << 2);			// x,y-axis mode; konfigurace - povolen� teplotn� kompenzace dat magnetometru
		writeByte(MAG, LSM9DS1M_CTRL_REG2_M, Mscale << 5 );								// rozsah magnetometru
		writeByte(MAG, LSM9DS1M_CTRL_REG3_M, 0x80 );									// "continuous conversion mode"
		writeByte(MAG, LSM9DS1M_CTRL_REG4_M, Mmode << 2 );								// povolena z-osa
		writeByte(MAG, LSM9DS1M_CTRL_REG5_M, 0x40 );									// povolen "block update mode"
		_delay_ms(200);

		printf("\t **********LSM9DS1 nastaveno a zkalibrov�no**********\n\n");
	}
}

void getMres()										// zji�t�n� rozli�en� mag.
{
	switch (Mscale)
	{
		// 4 Gauss (00), 8 Gauss (01), 12 Gauss (10) a 16 Gauss (11)
		case MFS_4G:
		mRes = 4.0/32768.0;
		break;
		case MFS_8G:
		mRes = 8.0/32768.0;
		break;
		case MFS_12G:
		mRes = 12.0/32768.0;
		break;
		case MFS_16G:
		mRes = 16.0/32768.0;
		break;
	}
}

void getGres()										// zji�t�n� rozli�en� gyra
{
	switch (Gscale)
	{
		// 245 DPS (00), 500 DPS (01), a 2000 DPS  (11).
		case GFS_245DPS:
		gRes = 245.0/32768.0;
		break;
		case GFS_500DPS:
		gRes = 500.0/32768.0;
		break;
		case GFS_2000DPS:
		gRes = 2000.0/32768.0;
		break;
	}
}

void getAres()										// zji�t�n� rozli�en� akcelerometru
{
	switch (Ascale)
	{
		// 2 Gs (00), 16 Gs (01), 4 Gs (10), a 8 Gs  (11).
		case AFS_2G:
		aRes = 2.0/32768.0;
		break;
		case AFS_16G:
		aRes = 16.0/32768.0;
		break;
		case AFS_4G:
		aRes = 4.0/32768.0;
		break;
		case AFS_8G:
		aRes = 8.0/32768.0;
		break;
	}
}

void readAccelData(int16_t *destination)			// �ten� dat z akcelerometru (x,y,z)
{
	uint8_t rawData[6];											// pole pro data x/y/z registr� (MSB a LSB) akcelerometru
	
	readBytes(AG, LSM9DS1XG_OUT_X_L_XL, 6, &rawData[0]);		// �ten� v�ech 6B x/y/z LSB a MSB
	
	destination[0] = ((int16_t)rawData[1] << 8) | rawData[0];
	destination[1] = ((int16_t)rawData[3] << 8) | rawData[2];
	destination[2] = ((int16_t)rawData[5] << 8) | rawData[4];	// oto�en� MSB a LSB, spojen� a odstran�n� 2. dopl�ku
}

void readGyroData(int16_t *destination)				// �ten� dat z gyra (x,y,z)
{
	uint8_t rawData[6];											// pole pro data x/y/z registr� (MSB a LSB) gyroskopu
	
	readBytes(AG, LSM9DS1XG_OUT_X_L_G, 6, &rawData[0]);			// �ten� v�ech 6B x/y/z LSB a MSB
	
	destination[0] = ((int16_t)rawData[1] << 8) | rawData[0];
	destination[1] = ((int16_t)rawData[3] << 8) | rawData[2];
	destination[2] = ((int16_t)rawData[5] << 8) | rawData[4];	// oto�en� MSB a LSB, spojen� a odstran�n� 2. dopl�ku
}

void readMagData(int16_t *destination)				// �ten� dat z magnetometru (x,y,z)
{
	uint8_t rawData[6];											// pole pro data x/y/z registr� (MSB a LSB) magnetometru
	
	readBytes(MAG, LSM9DS1M_OUT_X_L_M_MULTI, 6, &rawData[0]);	// �ten� v�ech 6B x/y/z LSB a MSB
	
	destination[0] = ((int16_t)rawData[1] << 8) | rawData[0];
	destination[1] = ((int16_t)rawData[3] << 8) | rawData[2];
	destination[2] = ((int16_t)rawData[5] << 8) | rawData[4];	// oto�en� MSB a LSB, spojen� a odstran�n� 2. dopl�ku
}

int16_t readTempData()								// �ten� teploty z LSM9DS1
{
	uint8_t rawData[2];											// pole pro LSB a MSB byte teploty
	readBytes(AG, LSM9DS1XG_OUT_TEMP_L, 2, &rawData[0]);		// �ten� z registru teploty
	return (((int16_t)rawData[1] << 8) | rawData[0]);			// oto�en� MSB a LSB, spojen� a odstran�n� 2. dopl�ku
}

void selftestLSM9DS1()								// selftest LSM9DS1 (acc, gyro)
{
	float accel_noST[3] = {0., 0., 0.}, accel_ST[3] = {0., 0., 0.};
	float gyro_noST[3] = {0., 0., 0.}, gyro_ST[3] = {0., 0., 0.};

	writeByte(AG, LSM9DS1XG_CTRL_REG10,   0x00);							// zak�zat self test
	accelgyrocalLSM9DS1(gyro_noST, accel_noST, 250);
	writeByte(AG, LSM9DS1XG_CTRL_REG10,   0x05);							// povolit gyro/accel self test
	accelgyrocalLSM9DS1(gyro_ST, accel_ST, 250);

	float gyrodx = (gyro_ST[0] - gyro_noST[0]);
	float gyrody = (gyro_ST[1] - gyro_noST[1]);
	float gyrodz = (gyro_ST[2] - gyro_noST[2]);

	printf("Gyro self-test v�sledek:\n");
	printf("\t x-axis = %f dps | m�lo by b�t v rozmez� 20 a� 250 dps\n", gyrodx);
	printf("\t y-axis = %f dps | m�lo by b�t v rozmez� 20 a� 250 dps\n", gyrody);
	printf("\t z-axis = %f dps | m�lo by b�t v rozmez� 20 a� 250 dps\n\n", gyrodz);

	float accdx = 1000.*(accel_ST[0] - accel_noST[0]);
	float accdy = 1000.*(accel_ST[1] - accel_noST[1]);
	float accdz = 1000.*(accel_ST[2] - accel_noST[2]);

	printf("Akcelerometr self-test v�sledek:\n");
	printf("\t x-axis = %f mg | m�lo by b�t v rozmez� 60 a� 1700 mg\n", accdx);
	printf("\t y-axis = %f mg | m�lo by b�t v rozmez� 60 a� 1700 mg\n", accdy);
	printf("\t z-axis = %f mg | m�lo by b�t v rozmez� 60 a� 1700 mg\n\n", accdz);
	
	writeByte(AG, LSM9DS1XG_CTRL_REG10, 0x00);								// zak�zat self test
	_delay_ms(200);
}

void accelgyrocalLSM9DS1(float * dest1, float * dest2, uint16_t vzorku_gyra)	// fce pro kompenzaci offsetu gyra a acc
{
	uint8_t data[6] = {0, 0, 0, 0, 0, 0};
	int8_t c;
	int32_t gyro_bias[3] = {0, 0, 0}, accel_bias[3] = {0, 0, 0};
	uint16_t ii, oo, samples = 0, time = 0;

	writeByte(AG, LSM9DS1XG_CTRL_REG4, 0x38);									// povolen� 3-axes mod gyra
	writeByte(AG, LSM9DS1XG_CTRL_REG1_G, Godr << 5 | Gscale << 3 | Gbw);		// dal�� nastaven� gyra
	//_delay_ms(200);
	writeByte(AG, LSM9DS1XG_CTRL_REG5_XL, 0x38);								// povolen� 3-axes mod acc
	writeByte(AG, LSM9DS1XG_CTRL_REG6_XL, Aodr << 5 | Ascale << 3 | 0x04 |Abw);	// dal�� nastaven� acc
	//_delay_ms(200);
	writeByte(AG, LSM9DS1XG_CTRL_REG8, 0x44);									// povolen� "block update" a v�cen�sobn�ho �ten� v�st. reg.
	
	_delay_ms(200);
	
	// pro kalibraci gyroskopu je p�esn�j�� pou��t fci gyrocalLSM9DS1 s libovoln� nastaviteln�m po�tem vzor� v�t��m jak 32!!!
	switch (Godr)																// v�po�et �asu pot�ebn�ho pro z�sk�n� nov�ho vzorku v us/10 (abych nemusel zbyte�n� pou��vat v�t�� typ uint32_t)
	{
		case GODR_14_9Hz:
		time = 6800;
		break;
		case GODR_59_5Hz:
		time = 1700;
		break;
		case GODR_119Hz:
		time = 850;
		break;
		case GODR_238Hz:
		time = 430;
		break;
		case GODR_476Hz:
		time = 220;
		break;
		case GODR_952Hz:
		time = 110;
		break;
	}

	for(ii = 0; ii < vzorku_gyra ; ++ii)										// pocet_vzorku_gyra -> po�adovan� po�et vzork� pro pr�m�rov�n� offsetu
	{
		if (readByte(AG, LSM9DS1XG_STATUS_REG) & 0x02)							// �ek�n� na dal�� vzorek
		{
			++samples;															// pro jistotu kontrola skute�n�ho po�tu z�skan�ch vzork�
			int16_t gyro_temp[3] = {0, 0, 0};
			readGyroData(gyro_temp);
			
			gyro_bias[0] += (int32_t) gyro_temp[0];
			gyro_bias[1] += (int32_t) gyro_temp[1];
			gyro_bias[2] += (int32_t) gyro_temp[2];								// sou�et v�ech vzork� pro x,y,z osy
			
			for(oo = 0; oo < time ; ++oo)										// smy�ka pro �ek�n� 10*time us na nov� vzorky (delay bere jen konstanty a n� prom�nn�, tak proto p�es for...jsem unaven� na vym��len� lep��ch v�c�...jsou t�i r�no)
			{
				_delay_us(10);
			}
		}
	}

	dest1[0] = ((float) gyro_bias[0]/samples)*gRes;
	dest1[1] = ((float) gyro_bias[1]/samples)*gRes;
	dest1[2] = ((float) gyro_bias[2]/samples)*gRes;

	_delay_ms(50);
	writeByte(AG, LSM9DS1XG_FIFO_CTRL, 0x00);									// povolit "gyro bypass mode"
	
	// akcelerometr bias
	c = readByte(AG, LSM9DS1XG_CTRL_REG9);
	writeByte(AG, LSM9DS1XG_CTRL_REG9, c | 0x02);								// povolit acc FIFO
	_delay_ms(50);																// prodleva ne� se v�e nastav� a zap�e
	writeByte(AG, LSM9DS1XG_FIFO_CTRL, 0x20 | 0x1F);							// povolit FIFO stream mode a na��st 32 vzork�
	_delay_ms(500);																// sb�r vzork�, �ek�n�
	
	samples = (readByte(AG, LSM9DS1XG_FIFO_SRC) & 0x2F);						// �te hodnotu skute�n�ho po�tu vzork�?

	for(ii = 0; ii < samples ; ii++)											// �eten� z FIFO acc
	{											
		int16_t accel_temp[3] = {0, 0, 0};
		readBytes(AG, LSM9DS1XG_OUT_X_L_XL, 6, &data[0]);
		accel_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]);
		accel_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]);
		accel_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]);			// sestaven� 16-bit int� x,y,z z FIFO dat k pr�m�rov�n�

		accel_bias[0] += (int32_t) accel_temp[0];
		accel_bias[1] += (int32_t) accel_temp[1];
		accel_bias[2] += (int32_t) accel_temp[2];								// sou�et v�ech vzork� pro x,y,z osy
	}

	accel_bias[0] /= samples;
	accel_bias[1] /= samples;
	accel_bias[2] /= samples;													// dokon�en� pr�m�rov�n� dat pod�len�m po�tem vzork�
	
	if(accel_bias[2] > 0L) {accel_bias[2] -= (int32_t) (1.0f/aRes);}				// odstran�n� vlivu gravitace na v�po�et offset�
	else {accel_bias[2] += (int32_t) (1.0f/aRes);}
	
	dest2[0] = (float)accel_bias[0]*aRes;										// dopo��t�n� p�esn�ch dat podle nastaven�ho rozli�en�/rozsahu acc (g)
	dest2[1] = (float)accel_bias[1]*aRes;
	dest2[2] = (float)accel_bias[2]*aRes;
	
	c = readByte(AG, LSM9DS1XG_CTRL_REG9);
	writeByte(AG, LSM9DS1XG_CTRL_REG9, c & ~0x02);								// zak�zat accel FIFO
	_delay_ms(50);
	writeByte(AG, LSM9DS1XG_FIFO_CTRL, 0x00);									// povolit "acc bypass mode"
}

void magcalLSM9DS1(float * dest1)					// fce pro kompenzaci offsetu mag
{
	uint8_t data[6];															// pole dat mag x,y,z
	uint16_t ii = 0, sample_count = 0;
	int32_t mag_bias[3] = {0, 0, 0};
	int16_t mag_max[3] = {0, 0, 0}, mag_min[3] = {0, 0, 0};
	uint8_t jj = 0;
	
	// konfigurace registr� magnetometru
	writeByte(MAG, LSM9DS1M_CTRL_REG1_M, 0x80 | Mmode << 5 | Modr << 2);		// povolen� x,y osy
	writeByte(MAG, LSM9DS1M_CTRL_REG2_M, Mscale << 5 );							// nastaven� rozsahu
	writeByte(MAG, LSM9DS1M_CTRL_REG3_M, 0x80 );								// nastaven� "continuous conversion mode"
	writeByte(MAG, LSM9DS1M_CTRL_REG4_M, Mmode << 2 );							// povolen� z osy
	writeByte(MAG, LSM9DS1M_CTRL_REG5_M, 0x40 );								// nastaven� "block update mode" dat
	
	printf("ot��ejte quadrou okolo os po dobu kalibrace!\n");
	_delay_ms(3000);
	
	sample_count = 500;
	for(ii = 0; ii < sample_count; ii++)
	{
		int16_t mag_temp[3] = {0, 0, 0};
		readBytes(MAG, LSM9DS1M_OUT_X_L_M_MULTI, 6, &data[0]);					// Read the six raw data registers into data array
		mag_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]);
		mag_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]);
		mag_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]);			// sestaven� 16-bit int� x,y,z k pr�m�rov�n�
		for (jj = 0; jj < 3; jj++)
		{
			if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
			if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
		}
		_delay_ms(55);															// p�i 20 Hz ODR, jsou nov� data dostupn� ka�d�ch cca 50 ms
	}

	mag_bias[0]  = (mag_max[0] + mag_min[0])/2;									// pr�m�r pro osu x mag z max-min hodnoty
	mag_bias[1]  = (mag_max[1] + mag_min[1])/2;									// pr�m�r pro osu y mag z max-min hodnoty
	mag_bias[2]  = (mag_max[2] + mag_min[2])/2;									// pr�m�r pro osu z mag z max-min hodnoty
	
	dest1[0] = (float) mag_bias[0]*mRes;										// dopo��t�n� p�esn�ch dat podle nastaven�ho rozli�en�/rozsahu mag (G)
	dest1[1] = (float) mag_bias[1]*mRes;
	dest1[2] = (float) mag_bias[2]*mRes;

	//z�pis offset� do offset registr� magnetometru;
	writeByte(MAG, LSM9DS1M_OFFSET_X_REG_L_M, (int16_t) mag_bias[0]  & 0xFF);
	writeByte(MAG, LSM9DS1M_OFFSET_X_REG_H_M, ((int16_t)mag_bias[0] >> 8) & 0xFF);
	writeByte(MAG, LSM9DS1M_OFFSET_Y_REG_L_M, (int16_t) mag_bias[1] & 0xFF);
	writeByte(MAG, LSM9DS1M_OFFSET_Y_REG_H_M, ((int16_t)mag_bias[1] >> 8) & 0xFF);
	writeByte(MAG, LSM9DS1M_OFFSET_Z_REG_L_M, (int16_t) mag_bias[2] & 0xFF);
	writeByte(MAG, LSM9DS1M_OFFSET_Z_REG_H_M, ((int16_t)mag_bias[2] >> 8) & 0xFF);
	
	printf("Kalibrace magnetometru dokon�ena\n");
}

void prepocet_dat()									//p�epo�et g -> m/s^2 a G -> mT
{
	axp = ax*9.81275;
	ayp = ay*9.81275;
	azp = az*9.81275;									// p�epo��t� zrychlen� z g -> m/s^2
	
	mxp = mx*100.0;
	myp = my*100.0;
	mzp = mz*100.0;										// propo��t� mag. indukci G -> uT
}

void LSM9DS1_main_fce()
{	
	int16_t accelCount[3], gyroCount[3], magCount[3];	// pole 16-bit signed acc, gyra, mag sensor - v�stupn� data
	int16_t tempCount;									// v�stupn� data teploty
	uint32_t Now = 0;								// Prom�nn� pro do�asn� ulo�en� us, kter� uplynuly od zapnut� a inicializace Xmegy
	float deltat = 0.0f;								// integra�n� interval pro Madgwick/Mahony filtr 
	
	if (readByte(AG, LSM9DS1XG_STATUS_REG) & 0x01)
	{																			// check if new accel data is ready
		readAccelData(accelCount);												// Read the x/y/z adc values
	 
		// Now we'll calculate the accleration value into actual g's
		ax = (float)accelCount[0]*aRes - accelBias[0];  // get actual g value, this depends on scale being set
		ay = (float)accelCount[1]*aRes - accelBias[1];
		az = (float)accelCount[2]*aRes - accelBias[2];
	}
 
	if (readByte(AG, LSM9DS1XG_STATUS_REG) & 0x02)
	{  // check if new gyro data is ready
		readGyroData(gyroCount);  // Read the x/y/z adc values

		// Calculate the gyro value into actual degrees per second
		gx = (float)gyroCount[0]*gRes - gyroBias[0];  // get actual gyro value, this depends on scale being set
		gy = (float)gyroCount[1]*gRes - gyroBias[1];
		gz = (float)gyroCount[2]*gRes - gyroBias[2];
	}
	 
		//if (readByte(MAG, LSM9DS1M_STATUS_REG_M) & 0x08)
		//{  // check if new mag data is ready
			//readMagData(magCount);  // Read the x/y/z adc values
		  //
			//// Calculate the magnetometer values in milliGauss
			//// Include factory calibration per data sheet and user environmental corrections
			//mx = (float)magCount[0]*mRes - magBias[0];  // get actual magnetometer value, this depends on scale being set
			//my = (float)magCount[1]*mRes - magBias[1];
			//mz = (float)magCount[2]*mRes - magBias[2];
		 //}
	 
	if (readByte(AG, LSM9DS1XG_STATUS_REG) & 0x03)
	{
		tempCount = readTempData();  // Read the gyro adc values
		temperature = ((float) tempCount/256. + 25.0); // Gyro chip temperature in degrees Centigrade
	}
  
	Now = count_us();
	deltat = (((float)Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
	if (deltat < 0.000002f) // o�et�en� z�porn�ho integra�n�ho intervalu
	{
		Now = count_us();
		deltat = ((Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
	}
	lastUpdate = Now;
	
	//************* pro tisk +3s po int
	if (pomoc_t == 1)
	{
		pomoc_t = Now + 1000000.0f;
	}  
	//*************
	  
	if (filtr == 1)
	{
		MadgwickAHRSupdate((gx*PI)/180.0f, (gy*PI)/180.0f, (gz*PI)/180.0f, -ax, -ay, az, -mx, my, mz, deltat);
	}
	else if (filtr == 2)
	{	
		MahonyAHRSupdate((gx*PI)/180.0f, (gy*PI)/180.0f, (gz*PI)/180.0f, -ax, -ay, az, -mx, my, mz, deltat);
	}

	yaw   = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
    pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
    roll  = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
    pitch *= 180.0f/PI;
    yaw   *= 180.0f/PI;
    yaw   += 4.15f;																// �r - Brno, 16.1.2016 http://www.magnetic-declination.com, http://www.ngdc.noaa.gov/geomag-web/#declination
    roll  *= 180.0f/PI;
	
//yaw = 0; //***********

			
			test_vyp(deltat);
			
			++smycka; 
			if (smycka > 15)
			{
				
				//********** tisk pro sestaven� graf� v Excelu
				//printf("%.5f\t%.5f\t%u\t%u\n\n", roll, pitch, zes, uhel);
				//printf("%.5f\t%.5f\t%.5f\t%lu\n", gx, gy, gz, Now);
				//printf("%.5f\t%.5f\t%.5f\t%lu\n", -mx, my, mz, Now);
				
				//********** tisk pro grafy v Serial Chart v CSV form�tu
		//printf("%.3f,%.3f,%.3f\n", roll, pitch, yaw);
				//printf("%.3f,%.3f,%u,%u,%.2f,%.3f,%.3f,%.3f\n", roll, pitch, tah, zes, beta, linPosHP[0], linPosHP[1], linPosHP[2]);
				//printf("%.3f,%.3f,%u,%u,%.2f\n", roll, pitch, tah, zes, beta);
				//printf("%.3f,%.3f,%.2f\n", roll, pitch, Fi_x);								// testov�n� P regul�toru v ose x
				//printf("%u\n", tah);
				
				smycka = 0;
		}
//*******************************************
 }
 
void test_vyp(float deltat)
{
	float linVel[3] ={0.0f, 0.0f, 0.0f};
	//float linAcc[3] = {0.0f, 0.0f, 0.0f};
	float linVelHP[3] = {0.0f, 0.0f, 0.0f};
	float linPos[3] = {0.0f, 0.0f, 0.0f};
	float linPosHP[3] = {0.0f, 0.0f, 0.0f};
	uint8_t i = 0;
	uint8_t j = 0;
	float acc[3];
	float tcAcc[3] = {0.0f, 0.0f, 0.0f};
		
	acc[0] = -ax;
	acc[1] = -ay;
	acc[2] = az;
	
	quatern2rotMat();		//vypo��t� matici rotace z quat.
	
	for (i = 0; i < 3; ++i)
	{
		for (j = 0; j < 3; ++j)
		{
			tcAcc[i] += R[i][j] * acc[j];
		}
	}											// for cykly spo��taly zrychlen� vzhledem k nato�en� v��i zemi
	
	//linAcc[2] = tcAcc[2] - 1;								// (p�eps�no n�) ode�ten� vlivu 1g => gravitace (pouze v ose z)
	linAcc[0] = tcAcc[0] * 9.81275;
	linAcc[1] = tcAcc[1] * 9.81275;							//g = 9,80665 m/s^2 - 45�s� (norm�ln� t�hov� zrychlen�), Brno g = 9,81275 m/s^2
	linAcc[2] = (tcAcc[2] - 1) * 9.81275;										// p�evod z g => m/s^2
	
	linVel[0] += linAcc[0]*deltat;
	linVel[1] += linAcc[1]*deltat;
	linVel[2] += linAcc[2]*deltat;
	
	//***HP filtr linVel?!?!
	linVelHP[0] = linVel[0];
	linVelHP[1] = linVel[1];
	linVelHP[2] = linVel[2];
	
	linPos[0] += linVelHP[0]*deltat;
	linPos[1] += linVelHP[1]*deltat;
	linPos[2] += linVelHP[2]*deltat;
	
	//***HP filtr linPos?!?!
	linPosHP[0] = linPos[0];
	linPosHP[1] = linPos[1];
	linPosHP[2] = linPos[2];
	
}

void quatern2rotMat()
{
	R[0][0] = 2*q[0]*q[0]-1+2*q[1]*q[1];
	R[1][0] = 2*(q[1]*q[2]+q[0]*q[3]);
	R[2][0] = 2*(q[1]*q[3]-q[0]*q[2]);
	
	R[0][1] = 2*(q[1]*q[2]-q[0]*q[3]);
	R[1][1] = 2*q[0]*q[0]-1+2*q[2]*q[2];
	R[2][1] = 2*(q[2]*q[3]+q[0]*q[1]);
	
	R[0][2] = 2*(q[1]*q[3]+q[0]*q[2]);
	R[1][2] = 2*(q[2]*q[3]-q[0]*q[1]);
	R[2][2] = 2*q[0]*q[0]-1+2*q[3]*q[3];
}