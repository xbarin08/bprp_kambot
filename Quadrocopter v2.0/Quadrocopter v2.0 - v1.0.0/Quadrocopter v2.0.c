/*										  Program Quadrocopter v1.4
												Vojt�ch Ham��ek										
 -----------------------------------------------------------------------------------------------------------------*/

#include <avr/sleep.h>										// re�imy sp�nku...
#include <stdlib.h>											// generov�n� ��sel, v�po�ty...
#include "Basic_Interrupts.h"								// p�eru�en� pro T/C a ext. int. pin� (vyp/zap tla��tko DPS)
#include "Regulators.h"										// Regulace

extern int16_t NavData[6];
uint32_t Cas_LED2 = 0;

void errs()											// kontroluje pole err a v p��pad� n�jak� chyby zap�e "p��znak" chyby do err[0] = po�et chyb
{
	uint8_t i = 0;
		
	err[errors] = 0;									// vynulov�n� "p��znaku" chyby

	for (i = 0; i < sizeof(err); ++i)					// test v�ech error� a err[0] = po�et chyb
	{
		if (err[i] > 0)
		{
			++err[errors];
		}
	}
}

void errstisk()										// tisk chybov�ch hl�en�
{
	uint8_t i = 0;
	for (i = 0; i < sizeof(err); ++i)
	{
		printf("\t\t error index %u = %u\n", i, err[i]);	// smy�ka tisku pole err
	}								
}

ISR(TCD0_OVF_vect)									// funkce p�eru�en� pro TCD0
{
	PP_reg_rot_XY();							// vol�n� regulace rotace
}

int main()											// hlavn� ��st programu
{	
	uint32_t Now = 0;									// Prom�nn� pro do�asn� ulo�en� us, kter� uplynuly od zapnut� a inicializace Xmegy
	
	initIO();										// fce. inicializace IO pin�
	remap();										// fce. p�emapov�n� UARTu atd.
	setUpSerial();									// fce. nastaven� UARTu
	setUp16MhzExternalOsc();						// fce. nastaven� krystalu
	setuptimerF0();									// fce. nastaven� PWM pro serva/motory
	setuptimerC0();									// fce. nastaven� timeru C0 (vyvol�n� p�eru�en� pro iLED)
	setuptimerC1();									// fce. nastaven� PWM pro LED1 a LED2 (1kHz)
	InitLSM9DS1();									// fce. nastaven� akcelerometru, gyra a magnetometru na SPI
	initI2C();										// fce. nastaven� I2C sb�rnice
	Init_LPS25H();									// fce. nastaven� barometru LPS25H
	
	errs();											// kontroluje pole err a v p��pad� n�jak� chyby zap�e "p��znak" chyby do err[0] = po�et chyb 
	
	INFO_TISK;										// info - verze, projekt atd.
	errstisk();										// tisk chybov�ch hl�en�
	
	clk_init();										// fce. ��t�n� po�tu us uplynul�ch od zapnut� a dokon�en� inicializace Xmegy
	
	setuptimerD0();									// provizorn� vol�n� letov� fce p�es interrupt TCD0
	
	INFO_TISK;

	while(1)										// nekone�n� smy�ka hlavn�ho programu
	{	
		if (NavData[4] > 250)						// zapnut�/vypnut� motor� �e�eno p�es podm�nku pro p��ku kan�lu 5 RC soupravy
		{
			P_S(CLR);
		}
		else if (NavData[4] < -250)
		{
			P_S(SET);
		}
		
		if (NavData[5] > 250)						// p�ep�n�n� re�imu LED2 p��kou kan�lu 6 RC soupravy
		{
			LED2 = 6500;							// p��ka nahoru = max svit
		}
		else if (NavData[5] > -250 && NavData[5] < 250)// p�ep�n�n� re�imu LED2 p��kou kan�lu 6 RC soupravy
		{
			LED2 = 7996;							// p��ka uprost�ed = min svit (zhasnut�)
		}
		else if (NavData[5] < -250)					// p��ka dol� = blik�n� 1 Hz se st��dou 1 %
		{
			Now = count_us();
													// vr�t� �as od zapnut� v us
			if ((Now - Cas_LED2) > 1000000 && LED2 > 0)
			{
				LED2 = 0;
				Cas_LED2 = Now;
			} 
			else if ((Now - Cas_LED2) > 10000 && LED2 == 0)
			{
				LED2 = 7996;
				Cas_LED2 = Now;
			}
		}
		
		//_delay_ms(40);
		//I2C_read_multi(LPS25H, LPS25H_PRESS, 3);
	}
}