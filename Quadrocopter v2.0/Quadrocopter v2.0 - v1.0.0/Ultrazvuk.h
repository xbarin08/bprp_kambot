/*
 * Ultrazvuk.h
 */ 


#ifndef ULTRAZVUK_H_
#define ULTRAZVUK_H_

#include "Init_Interfaces.h"								// sb�rnice (UART, SPI,...)

#define SRF10			0xE0								// I2C adresa SRF10
#define SRF10_CR		0x00								// I2C registr SRF10 - command reg.
#define SRF10_GAIN		0x01								// I2C registr SRF10 - gain reg.
#define SRF10_RANGE		0x02								// I2C registr SRF10 - range reg.

#endif /* ULTRAZVUK_H_ */