/*
 * Basic_Interrupts.c
 */ 

#include "Basic_Interrupts.h"
#include "twi_master_driver.h"						// Master I2C

int16_t NavData[6] = {0,0,0,0,0,0};					// naviga�n� data z RC soupravy (glob�ln� - chci je m�t v�dy aktu�ln� a v�ude k dispozici!!!)
uint32_t TempNow = 0.0f;							// prom�nn� pro ulo�en� mezi�asu
uint8_t RC_Last = 0;								// prom�nn�, kter� uchov�v�, ze kter�ho kan�lu p�i�el posledn� ��dic� pulz z RC soupravy...

ISR(TCC0_OVF_vect)									// funkce p�eru�en� pro TCC0
{
	iLED(TGL);										// neguje hodnotu pinu B2 (indika�n� LED)
}	

ISR(PORTF_INT0_vect)								// Fce pro obsluhu zap�nac�ho tla��tka DPS (EXT. INT0 na PF)
{
	char i = 0;										// pomocn� prom�nn� i pro for cyklus
	TC_SetPeriod(&TCC0, 3905);						// nastaven� f = 4Hz pro indika�n� LED
	TCC0.CNT = 0x00;								// reset TCC0
	
	for (i = 0; i <= 200; i++)						// for cyklus ��taj�c� 200*20ms = 4s
	{
		_delay_ms(20);								// prodleva 20ms*200 = 4s
		if (PORTF.IN & (1<<6))						// pokud je PF6 = 1 (tzn. je stisknuto zap�nac� tla��tko DPS)
		{
			if (i >= 200)							// pokud je i v�t�� nebo rovno 250 (tzn. ub�hlo 4 sekund)
			{
				PWR_ON(CLR);							// Vypnout nap�jen� DPS
				TCC0.CTRLA = 0;							// Prescaler N = 0 (tzn. vypnout TCC0 pro blik�n� indika�n� LED)
				iLED(SET);								// PB2 = 1 (tzn. indika�n� LED je rozsv�cen�...dokud nepust�m tla��tko)
			}
		}
		else										// pokud bylo tla��tko pu�t�no p�ed uplynut�m 4 sekund
		{
			TC_SetPeriod(&TCC0, 15624);				// vr�tit zp�t f = 1Hz indika�n� LED
			TCC0.CNT = 0x00;						// reset TCC0
			break;									// ukon�it for cyklus
		}
	}
}

ISR(PORTB_INT0_vect)								// Fce pro obsluhu RC ovlada�e(EXT. INT0 na PB)
{	
	int16_t Data = 0;								// pomocn� datov� prom�nn� pro test dat, jestli jsou cca +-500 [-]
	uint32_t Now = 0;						// Prom�nn� pro do�asn� ulo�en� us, kter� uplynuly od zapnut� a inicializace Xmegy
	
	Now = count_us();								// vr�t� �as od zapnut� v us
	
	Data = (Now - TempNow) - 1500;					// data o velikosti 1000 us a� 2000 us se st�edem uprost�ed...p�epo�te na +- 500 us
	
	if (Data > -600 && Data < 600)					// test dat...pro jistotu trochu �ir�� => +-600 [-]
	{
		NavData[RC_Last] = Data;					// zap�e rozd�l �as� (d�lku pulzu) dan�ho kan�lu
	}	
			
	if (RC_Last == 5)								// pokud byl konec datov�ho r�mce a detekovala se sestupn� hrana, potom se mus� zase vr�tit detekce n�b�n� pro nov� r�mec
	{
		PORTB.PIN6CTRL = PORT_ISC_RISING_gc;
	}
	
	if (PORTB.IN & (1<<0))							// pokud je sign�l z RC na pinu 0, potom...zde je aktu�ln� kan�l 2, ale ��slov�n� je posunut� o jedno n� kv�li 0 prvku pole
	{	
		RC_Last = 1;								// prost� tyhle ��len� podm�nky okolo testuj�, kter� pin vyvolal p�eru�en�
	}
	if (PORTB.IN & (1<<3))	
	{
		RC_Last = 2;
	}
	if (PORTB.IN & (1<<4))
	{
		RC_Last = 0;
	}
	if (PORTB.IN & (1<<5))
	{
		RC_Last = 3;
	}
	if (PORTB.IN & (1<<6))
	{
		RC_Last = 5;
		PORTB.PIN6CTRL = PORT_ISC_FALLING_gc;			// p�ed detekci konce datov�ho r�mce se mus� p�ej�t na detekci sestupn� hrany
	}
	if (PORTB.IN & (1<<1))
	{
		RC_Last = 4;
	}
								
	TempNow = Now;									// uchov� �as posledn� aktualizace stavu
}

/*ISR(USARTC0_RXC_vect)
{

}*/