/*
 * Init_Xmega.h
 */ 

#ifndef INIT_XMEGA_H_
#define INIT_XMEGA_H_

#define F_CPU			32000000UL							// 16 MHz krystal (vy�aduje to <util/delay.h>)...n�soben� na 32MHz (PLL = 2�)

#define REG2			TCF0.CCA							// Regul�tor motoru 2
#define REG1			TCF0.CCB							// Regul�tor motoru 1
#define REG3			TCF0.CCC							// Regul�tor motoru 3
#define REG4			TCF0.CCD							// Regul�tor motoru 4
#define LED1			TCC1.CCA							// LED1 (neg)
#define LED2			TCC1.CCB							// LED2 v�konov� (neg)
#define nap_5V(X)		PORTA.OUT ## X = PIN0_bm			// Ovl�dac� pin m�ni�e 5V (neg), pin PA0
#define iLED(X)			PORTB.OUT ## X = PIN2_bm			// indika�n� LED DPS
#define TL_PWR(X)		PORTF.OUT ## X = PIN6_bm			// Detekce stisku zap�nac�ho tla��tka DPS
#define PWR_ON(X)		PORTF.OUT ## X = PIN7_bm			// Zapnut�/vypnut� DPS
#define P_S(X)			PORTA.OUT ## X = PIN5_bm			// Zapnut� vypnut� DPS Q5 - light Power Switch, pin PA5
						// p�ejmenov�n� nepohodln�ch p�smenek a dal�� makra
			
#include <avr/io.h>											// vstupy a v�stupy pro dan� mikrokontrol�r...
#include <avr/interrupt.h>									// obsluha p�eru�en�...
#include <util/delay.h>										// prodlevy...
#include "avr_compiler.h"									// n�co �ivotn� d�le�it�ho od AVR, ale jsem moc ospalej, abych to te� proch�zel...
#include "TC_driver.h"										// Timer/Counter

void setUp16MhzExternalOsc();						// extern� krystal 16MHz
void setuptimerF0();								// PWM pro regul�tory vrtul�
void setuptimerC0();								// TCC0 = 1Hz s LowPriority int. pro indika�n� LED
void setuptimerD0();								// TCC0 = 450,045Hz s HiPriority int. pro funkce letu (do�asn� �e�en�)
void setuptimerE0();								// PWM 37,9kHz pro IR senzory/20Hz pro ultrazvuk
void setuptimerC1();								// funkce p�eru�en� pro LED1 a LED2 (negovan� ovl�d�n�)
void clk_init();									// fce ��t�n� po�tu uplynul�ch us od zapnut� Xmegy
uint32_t count_us();								// fce vracej�c� vypo�ten� po�et uplynul�ch us od zapnut� Xmegy
void initIO();										// nastaven� vstupn�-v�stupn�ch pin�

#endif /* INIT_XMEGA_H_ */