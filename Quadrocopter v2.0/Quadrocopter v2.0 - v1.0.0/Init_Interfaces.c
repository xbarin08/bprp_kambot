/*
 * Init_Interfaces.c
 */ 

#include "Init_Interfaces.h"

uint8_t SPIDir = 0;

void Spiopen()
{
	SPIDir = SPIPort.DIR;
	SPIPort.DIRCLR = (1<<SPIMI);
	SPIPort.DIRSET = (1<<SPIMO) | (1<<SPISC) | (1<<SPISS); // Make sure slave select is an output
	//SPIPort.OUTCLR = (1<<SPISS);
	SPICTRL.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_0_gc; //DIV4 pro 1MHz (| SPI_PRESCALER_DIV4_gc), aktu�ln� bez p�edd�li�ky jede na 8 MHz!!!
}

void Spiclose()
{
	SPICTRL.CTRL = 0;
	SPIPort.DIR = SPIDir;
	//SPIPort.OUTSET = (1<<SPISS);
	SPIPort.DIRSET = (1<<SPIMI);
}

int SpiReadWrite(char val)
{
	Spiopen();
	SPICTRL.DATA = val;
	while(!(SPICTRL.STATUS & (1<<7)));
	Spiclose();
	return SPICTRL.DATA;
}

void CS_SPI(char addr)								// SPI adresa (PE.4 pro CS_AG, PF.4 pro CS_M)
{
	if (addr == 0)										// pro adresu 0
	{
		CS_AG(TGL);										// povolen�/zak�z�n� komunikace - log.0/1 na PE.4 pro AG (negace)
	}
	else if (addr == 1)									// pro adresu 1
	{
		CS_M(TGL);										// povolen�/zak�z�n� komunikace - log.0/1 na PF.4 pro M (negace)
	}
}

void setUpSerial()									// nastaven� UARTu
{
	// nastaven� BaudRate (Bd)
	// BSEL = (32000000 / (2^0 * 16*500k) -1 = 1,... -> BSCALE = 0
	// FBAUD = ( (32000000)/(2^0*16(3+1)) = 1000k -> chyba 0%
	
	USARTC0_BAUDCTRLB = 0;								// nastaven� BSCALE = 0 ..."p�edd�li�ka pro nastaven� rychlosti"
	USARTC0_BAUDCTRLA = 0x0;							// 3 (0x03) pro 500k s chybou 0% (pro 250k s chybou 0% je to 7); (1 pro 1000k, 0 pro 2000k)
	//USARTC0.CTRLA = USART_RXCINTLVL_MED_gc;				// povolen� p�eru�en�
	USARTC0_CTRLC = USART_CHSIZE_8BIT_gc;				// 8b, bez parity, 1 stop bit
	USARTC0_CTRLB = USART_TXEN_bm | USART_RXEN_bm;		// povolen� komunikace
	sei();
}
 
int uart_putchar (char c, FILE *stream)		// odes�l�n� znak� printf na UART
{
	if (c == '\n')
	uart_putchar('\r', stream);
	
	while (!(USARTC0_STATUS & USART_DREIF_bm));			// �ek�n� na vypr�zdn�n� bufferu
	USARTC0_DATA = c;									// vlo�� char do bufferu
	
	return 0;											// pokud nedo�lo k chyb�, vr�t� 0
}

int uart_getchar(FILE *stream)						// �ten� znak� scanf z UARTu
{
	while( !(USARTC0_STATUS & USART_RXCIF_bm) );		// �ek� na p�ijet� znaku
	char data = USARTC0_DATA;							// do�asn� ulo�en� p�ijat�ch znak�
	if(data == '\r')
	data = '\n';
	uart_putchar(data, stream);							// "p�ijat� znak po�le na konzolu" - zp�tn� odesl�n�
	return data;										// vr�t� data - p�ijat� znaky
}
FILE usart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);		//printf a scanf pro UART

void remap()										// P�emapov�n� UARTu na port C
{
	// p�emapov�n� UARTu
	stdout = stdin = &usart_str;					// p�esm�rov�n� na UART (printf a scanf)
	
#ifdef VILIK_DPS_PC7_ERR							// USART0 na PORT C - p�esn�ji USART C0 (PLAT� POUZE PRO VILDOVU DPS S NEFUNK�N�M PC7 PINEM)
	PORTC_OUTSET = PIN3_bm;							// TX na PC3
	PORTC_DIRSET = PIN3_bm;							// TX pin jako output
	PORTC_OUTCLR = PIN2_bm;							// RX na PC2
	PORTC_DIRCLR = PIN2_bm;							// RX pin jako input
#else
	PORTC_REMAP |= 0x16;							// USART0 na PORT C - p�esn�ji USART C1
	PORTC_OUTSET = PIN7_bm;							// TX na PC7
	PORTC_DIRSET = PIN7_bm;							// TX pin jako output
	PORTC_OUTCLR = PIN6_bm;							// RX na PC6
	PORTC_DIRCLR = PIN6_bm;							// RX pin jako input
#endif
	
	// v�choz� nastaven� CS_AG a CS_M - zak�z�n� komunikace na SPI CS
	CS_AG(SET);										// High na E.4
	CS_M(SET);										// High na F.4
}

void initI2C()										// nastaven� I2C
{
	TWIC_MASTER_BAUD = 0x20;											//TWI frekvence: 100 kHz p�i 0x92, 390 kHz pro 0x20
	TWIC.MASTER.CTRLA = TWI_MASTER_ENABLE_bm | TWI_MASTER_WIEN_bp;		// povolen� I2C
	TWIC_MASTER_STATUS |= TWI_MASTER_BUSSTATE1_bp;						// nastaven� idle modu I2C
}

void I2C_write(char addr, char addr_reg, char value_reg)		// �ten� dat z I2C
{
	TWIC_MASTER_ADDR = addr;									// zasl�n� adresy I2C za��zen� + bit pro z�pis
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_WIF_bm));			// �ek� na odesl�n�
	
	TWIC_MASTER_DATA = addr_reg;								// zasl�n� adresy registru
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_WIF_bm));			// �ek� na odesl�n�
	
	TWIC_MASTER_DATA = value_reg;								// zasl�n� nov� hodnoty registru
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_WIF_bm));			// �ek� na odesl�n�
	TWIC.MASTER.CTRLC = TWI_MASTER_ACKACT_bm | TWI_MASTER_CMD_STOP_gc;
}

void I2C_read(char addr, char reg)					// adresov�n� I2C za��zen� pro �ten�
{
	TWIC_MASTER_ADDR = addr;									// zasl�n� adresy I2C za��zen� + bit pro z�pis
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_WIF_bm));			// �ek� na odesl�n�
	
	TWIC_MASTER_DATA = reg;										// zasl�n� adresy registru
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_WIF_bm));			// �ek� na odesl�n�
	
	TWIC_MASTER_ADDR = (addr+1);								// zasl�n� adresy I2C za��zen� + bit pro �ten�
	while(!(TWIC_MASTER_STATUS & TWI_MASTER_RIF_bm));			// �ek� na odesl�n�
}