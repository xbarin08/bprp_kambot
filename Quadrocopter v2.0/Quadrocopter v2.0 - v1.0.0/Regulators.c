/*
 * Regulators.c
 */ 

#include "Regulators.h"								// jeho hlavi�ka

unsigned int tah = 6500;
extern uint32_t pomoc_t;
extern int16_t NavData[6];
extern float gx, gy, gz;							// prom�nn� v�stupn�ch hodnot gyra( �/s)
extern float pitch, yaw, roll;						// prom�nn� pro vypo�ten� Eulerovy �hly
uint32_t Cas_Tah = 0;								// pro tvorbu prodlevy u v��ky


uint8_t zpom = 0;
extern float data;
extern float linAcc[3];
extern float q[4];
extern float az;

void PP_reg_rot_XY()
{
	float Reg_x, Reg_y, Reg_z_x, Reg_z_y, pom_z = 0;
	float Moment_x, Moment_y, Moment_z = 0;
	float F_x, F_y = 0;
	float Pozadovane_X = 0;
	float Pozadovane_Y = 0;
	float Pozad_rych_Z = 0;
	uint32_t Now = 0;									// Prom�nn� pro do�asn� ulo�en� us, kter� uplynuly od zapnut� a inicializace Xmegy
	
	LSM9DS1_main_fce();								// vol� senzory a zpracov�v� data scc, gyra a mag
	
	//************V�po�et v�sledn�ho tahu z RC******************
	
	Now = count_us();								// vr�t� �as od zapnut� v us
	
	if ((Now - Cas_Tah) > 100000)					// ka�d�ch 100 ms vypo�te nov� tah
	{
		
		if (NavData[2] > 400 && NavData[5] < -250)
		{
			tah = 10900;
		}
		
		if (NavData[2] < -75 || NavData[2] > 75)	// p�smo necitlivosti p��ek
		{
			tah += (NavData[2]/20);					// nov� tah
		}	
		if ( tah < 6500)
		{
			tah = 6500;
		} 
		else if ( tah > 12500)
		{
			tah = 12500;
		}
				
		Cas_Tah = Now;								// posledn� update �asu
	}
	
	//**********************n�klon X a Y************************
	
	Pozadovane_X = (NavData[3]/40);					// X bude v rozmez� +-10�
	Pozadovane_Y = (NavData[1]/40);					// Y bude v rozmez� +-10�
	Pozad_rych_Z = (NavData[0]/180);				// 
	
	//**********************************************************
	
	if (Now > 5000000 && Now > pomoc_t)				//�ek� na ust�len� filtru 5 mil. us = 5s nebo dobu po vyvol�n� int.
		{
			 Moment_x = (((Pozadovane_X - roll) * K1) - gx) * K2;
			 Moment_y = (((-Pozadovane_Y - pitch) * K1) - gy) * K2;
			 Moment_z = ((Pozad_rych_Z - gz) * K);
			
			 if (Moment_x > 0.15)
			 {
				 Moment_x = 0.15;
			 }
			 else if (Moment_x < -0.15)
			 {
				 Moment_x = -0.15;
			 }
			 
			 if (Moment_y > 0.15)
			 {
				 Moment_y = 0.15;
			 }
			 else if (Moment_y < -0.15)
			 {
				 Moment_y = -0.15;
			 }
			
			 if (Moment_z > 0.02)
			 {
				 Moment_z = 0.02;
			 }
			 else if (Moment_z < -0.02)
			 {
				 Moment_z = -0.02;
			 }
			 
			 F_x = Moment_x/L;
			 F_y = Moment_y/L;
			 
			 Reg_x = F_x/STRMOST;
			 Reg_y = F_y/STRMOST;
			 
			 if (tah >= 10500)
			 {
				pom_z = 0.00007562 * exp(0.00053872 * tah);
				Reg_z_x = 1845.3 * log((Moment_z/4) + pom_z) + 17575 - tah;
				Reg_z_y = 1845.3 * log((-Moment_z/4) + pom_z) + 17575 - tah;
			 } 
			 else
			 {
				 Reg_z_x = 0;
				 Reg_z_y = 0;
			 }
			 

			 REG1 = tah + Reg_y + Reg_z_y;
			 //REG1 = 6500; // jen pro lad�n� na ��ku
			 REG2 = tah - Reg_x + Reg_z_x;
			 REG3 = tah - Reg_y + Reg_z_y;
			//REG3 = 6500; // jen pro lad�n� na ��ku
			 REG4 = tah + Reg_x + Reg_z_x;
		}
		
		//++zpom;
		//if (zpom > 15)
		//{
			I2C_read_multi(LPS25H, LPS25H_PRESS, 3);
				I2C_write(LPS25H, LPS25H_CTRL_REG2, 0x01);//60 p�ipravit dal�� vzorek tlaku na p��t� �ten�
		//printf("%lu %.4f %.4f %.4f %.3f\n", Now, linAcc[0], linAcc[1], linAcc[2], data);
						//printf("%lu %.4f %.3f\n", Now, linAcc[2], data);   // testy v matlabu
				printf("%lu %.4f %.4f %.2f %.2f %.2f %.4f\n", Now, linAcc[2], data, pitch, roll, yaw, az);		
			//printf("%lu %.2f %.2f %.2f %d %.3f %.3f\n", Now, pitch, roll, yaw, tah, data, linAcc[2]);
			//printf("%d %d %d %d %d %d\n", NavData[0], NavData[1], NavData[2], NavData[3], NavData[4], NavData[5]);
		
		//zpom = 0;
		//}
}

ISR(USARTC0_RXC_vect)
{
	////cli();
	//
	////tah = 11000;
	////REG1 = tah;
	////REG2 = tah;
	////REG3 = tah;
	////REG4 = tah;
	//
	//char pom = 0;
	//unsigned int pom2 = 0;
	//char scanf_err = 0;
	//char UART_err;
	//
	//printf("\nfiltr = %u\n", filtr);
	//printf("strmost = %f\n", strmost);
	//printf("tah = %u\n", tah);
	//printf("k1 = %f\n", k1);
	//printf("k2 = %f\n", k2);
	//printf("beta = %f\n\n", beta);
	//
	//printf ("HELP:\n a -> v�b�r filtru \n b -> strmost \n c -> tah motor� (v�kon - ot��ky) \n d -> k1 \n e -> k2 \n f -> beta (�hel GyroMeanError) \n");
	//scanf ("%s", &pom);
	//
	//if (pom == 'a')
	//{
		//printf ("\t1 = Madgwick filtr\n\t2 = Mahony filtr\n");
		//
		//scanf_err = scanf ("%u", &filtr);
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'b')
	//{
		//printf ("\tZadejte strmost regulace motor�\n");
		//
		//scanf_err = scanf ("%u", &pom2);
		//
		//strmost = ((float)pom2/10000);
		//
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'c')
	//{
		//printf ("\tZadejte hodnotu tahu motor� >6500\n");
		//tah_last = tah;
		//scanf_err = scanf ("%u", &tah);
		//if (tah > 12100)
		//{
			//tah = 12100;
		//}
		//if (tah < 6500)
		//{
			//tah = 6500;
		//}
		//
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'd')
	//{
		//printf ("\tZadejte k1\n");
		//
		//scanf_err = scanf ("%u", &pom2);
		//k1 = ((float)pom2/100);
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'e')
	//{
		//printf ("\tZadejte k2\n");
		//
		//scanf_err = scanf ("%u", &pom2);
		//k2 = (float) pom2/1000;
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'f')
	//{
		//printf ("\tZadejte �hel beta\n");
		//
		//scanf_err = scanf ("%u", &uhel);
		//beta = sqrt(3.0f / 4.0f) * (PI * (uhel / 180.0f));
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'i')
	//{
		//printf ("\tIR_S\n");
		//
		//scanf_err = scanf ("%u", &ir_pom);
		//TCE0.CCA = ir_pom;
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'x')
	//{
		//printf ("\tZadejte �hel vych�len� ve �\n");
		//
		//scanf_err = scanf ("%u", &pom2);
		//Fi_x = pom2;
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'l')
	//{
		//printf ("\tZadejte intenzitu svitu LED od 0 do 7995\n");
		//
		//scanf_err = scanf ("%u", &pom2);
		//LED2 = 7996 - pom2;
		//if (scanf_err != 1)
		//{
			//scanf ("%s", &UART_err);
			//printf("Byly zad�ny �patn� hodnoty! (%c)\n", UART_err);
		//}
	//}
	//else if (pom == 'm')
	//{
		//P_S(TGL);
	//}
	//printf("\nfiltr = %u\n", filtr);
	//printf("strmost = %f\n", strmost);
	//printf("tah = %u\n", tah);
	//printf("k1 = %f\n", k1);
	//printf("k2 = %f\n", k2);
	//printf("beta = %f\n\n", beta);
	//
	//pomoc_t = 1;
	//
	//lastUpdate = count_us();
	//
	////sei();
}