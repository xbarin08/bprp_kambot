/*
 * LPS25H.h
 */ 


#ifndef LPS25H_H_
#define LPS25H_H_

#include "Init_Interfaces.h"								// sb�rnice (UART, SPI,...)

#define LPS25H				0xBA							// I2C adresa LPS25H - senzor tlaku (barometr)
#define LPS25H_PRESS		0xA8							// I2C registr LPS25H - LSB tlaku
#define LPS25H_TEMP			0x2C							// adresa MSB teploty LPS25H
#define LPS25H_CTRL_REG1	0x20							// konfigura�n� registr LPS25H
#define LPS25H_CTRL_REG2	0x21							// konfigura�n� registr LPS25H
#define LPS25H_RES_CONF		0x10							// konfigura�n� registr LPS25H
#define LPS25H_FIFO_CTRL	0x2E							// konfigura�n� registr LPS25H
#define LPS25H_WHO_AM_I		0x0F							// WhoAmI registr LPS25H

char I2C_Who_Am_I(char addr, int reg, int WhoAmI);			// I2C testov�n� WhoAmI registr� + vyhodnocen� err
void Init_LPS25H();											// konfigurace barometru LPS25H
float I2C_read_multi(char addr, int reg, int bytu);			// �ten� v�ce bajt� na I2C

#endif /* LPS25H_H_ */