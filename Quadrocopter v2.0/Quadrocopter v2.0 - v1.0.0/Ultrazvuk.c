/*
 * Ultrazvuk.c
 */ 

#include "Ultrazvuk.h"								// jeho hlavi�ka

//ISR(TCE0_OVF_vect)									// funkce p�eru�en� pro TCE0
//{
//unsigned int meziprom = 0;									// meziprom�nn� pro do�asn� ulo�en� p�ijat�ch dat
//float vyska_ultra = 0.0f;
//
//I2C_read(SRF10, SRF10_RANGE);
//meziprom = TWIC.MASTER.DATA << (8);									// zaplnit prvn�ch 8b meziprom�nn� prvn�m p�ijat�m B
//TWIC.MASTER.CTRLC = TWI_MASTER_CMD_RECVTRANS_gc;					// po prvn�m B odeslat ACK
//while(!(TWIC_MASTER_STATUS & TWI_MASTER_RIF_bm));					// �ek�n� na odesl�n�
//
//meziprom |= TWIC.MASTER.DATA;										// spodn�ch 8b meziprom zaplnit druh�m p�ijat�m B
//TWIC.MASTER.CTRLC = TWI_MASTER_ACKACT_bm | TWI_MASTER_CMD_STOP_gc;	// odeslat NAK, ukon�it komunikaci
//
//vyska_ultra = (meziprom * 0.017315f);
//ultrazvuk_prum(vyska_ultra);
//
//I2C_write(SRF10, SRF10_CR, 0x52);
//}

//void ultrazvuk_prum(float vyska_ultra)				// m��en� v��ky ultrazvukv�m senzorem - klouzav� pr�m�r (5 hodnot)
//{
//ultra[0] = ultra[1];
//ultra[1] = ultra[2];
//ultra[2] = ultra[3];
//ultra[3] = ultra[4];
//ultra[4] = ultra[5];
//ultra[5] = ultra[6];
//ultra[6] = ultra[7];
//ultra[7] = ultra[8];
//ultra[8] = ultra[9];
//ultra[9] = vyska_ultra;
//
//vyska_ultra_prum = (ultra[0] + ultra[1] + ultra[2] + ultra[3] + ultra[4] + ultra[5] + ultra[6] + ultra[7] + ultra[8] + ultra[9]) / 10.0f;
//}
//
//void initSRF10()										// nastaven�
//{
//I2C_write(SRF10, SRF10_CR, 0x52);
//_delay_ms(65);
//I2C_write(SRF10, SRF10_GAIN, 0x08);					// zes�len�
//_delay_ms(1);
//I2C_write(SRF10, SRF10_RANGE, 0x5D);				// max. rozsah => 400cm
//_delay_ms(1);
//I2C_write(SRF10, SRF10_CR, 0x52);
//_delay_ms(25);
//}