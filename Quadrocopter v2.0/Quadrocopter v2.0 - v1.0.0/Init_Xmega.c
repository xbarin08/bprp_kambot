/*
 * Init_Xmega.c
 */ 

#include "Init_Xmega.h"

void setUp16MhzExternalOsc()						// extern� krystal 16MHz
{
	OSC_XOSCCTRL = OSC_FRQRANGE_12TO16_gc | OSC_XOSCSEL_XTAL_16KCLK_gc;	// nastav� 16MHz extern� krystal
	OSC_CTRL |= OSC_XOSCEN_bm;											// povolen� ext. krystalu
	while(!(OSC_STATUS & OSC_XOSCRDY_bm));								// �ek�n� na stabilizaci krystalu
	
	OSC_PLLCTRL = OSC_PLLSRC_XOSC_gc | 2;								// PLL n�sobi�ka 2*16MHz = 32MHz
	OSC_CTRL |= OSC_PLLEN_bm;											// povolen� PLL...
	while(!(OSC_STATUS & OSC_PLLRDY_bm));								// �ek�n� na stabilizaci
	
	CCP = CCP_IOREG_gc;													// nastaven� syst�mov�ch hodin z ext. krystalu
	CLK_CTRL = CLK_SCLKSEL_PLL_gc;										// nastaven� syst. hodin s PLL
}

void setuptimerF0()									// PWM pro regul�tory vrtul�
{
	TCF0.PER = 17771;								// s p�edd�li�kou 0x03 => f = 449,99Hz
	REG1 = 6500;									// kompar�tor A (6500 "aktivuje" regul�tory)
	REG2 = 6500;									// kompar�tor B (6500 "aktivuje" regul�tory)
	REG3 = 6500;									// kompar�tor C (6500 "aktivuje" regul�tory)
	REG4 = 6500;									// kompar�tor D (6500 "aktivuje" regul�tory)
	TCF0.CTRLA =  0x03;								// P�edd�li�ka (N)
	TCF0.CTRLB =  0xf3;								// povol� porovn�v�n� kompar�tor� A,B,C,D v re�imu single slope pwm
	//TCF0.INTCTRLA =  0x01;						// povolen� p�eru�en� p�i p�ete�en�
}

void setuptimerC0()									// TCC0 = 1Hz s LoPriority int. pro indika�n� LED
{
	TC_SetPeriod(&TCC0, 15624);							// (32 000 000)/2*1024(555+1) = 1Hz (Top hodnota pro ��ta�)
	TCC0.CTRLA = TC_CLKSEL_DIV1024_gc;					// Prescaler N = 1024

	TC0_SetOverflowIntLevel(&TCC0, TC_OVFINTLVL_MED_gc);// nastaven� p�eru�en� p�i p�ete�en� TCC0
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;						// nastaven� n�zk� priority p�eru�en� TCC0
	sei();												// nastaven� grob�ln�ho p�eru�en�
}

void setuptimerD0()									// TCD0 = 400Hz s HiPriority int. pro funkce letu (do�asn� �e�en�)
{
	TCD0.CTRLB = TC_WGMODE_SS_gc;					// re�im timer/counter0 -> single slope mode
	TCD0.CTRLA = TC_CLKSEL_DIV64_gc;				// p�edd�li�ka N = 64
	TCD0.PER   = 1249;								// p�ete�en� p�i na��t�n� -> 400Hz
	TCD0.INTCTRLA = TC_OVFINTLVL_MED_gc;			// vysok� �rove� vyvol�n� p�eru�en�
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;					// nastaven� nejvy���� priority p�eru�en� TCC0
	sei();
}

//void setuptimerE0()									// PWM pro IRS
//{
	//TCE0.PER = 1000;									// s p�edd�li�kou 0x02 => f = 37,9kHz
	//TCE0.CCA = 990;									// kompar�tor A (50%)
	//TCE0.CCC = 990;									// kompar�tor A (50%)
	//TCE0.CTRLA =  0x01;								// P�edd�li�ka (N)
	//TCE0.CTRLB =  0xf3;								// povol� porovn�v�n� kompar�tor� A,B,C,D v re�imu single slope pwm
	////TCR0.INTCTRLA =  0x01;						// povolen� p�eru�en� p�i p�ete�en�
//}

void setuptimerE0()									// Interrupt pro ultrazvuk
{
	//TC_SetPeriod(&TCE0, 14284);							// (32 000 000)/...= 40Hz (Top hodnota pro ��ta�)
	//TCE0.CTRLA = TC_CLKSEL_DIV64_gc;					// Prescaler N = 64
//
	//TC0_SetOverflowIntLevel(&TCE0, TC_OVFINTLVL_MED_gc);// nastaven� p�eru�en� p�i p�ete�en� TCC0
	//PMIC.CTRL |= PMIC_MEDLVLEN_bm;						// povolen� st�edn� priority p�eru�en� TCC0
	//sei();
}

void setuptimerC1()									// funkce p�eru�en� pro LED1 a LED2 (negovan� ovl�d�n�)
{
	TCC1.PER = 7996;								// s p�edd�li�kou 0x03 => f = 1000,000Hz
	LED1 = 7996;									// kompar�tor A
	LED2 = 7996;									// kompar�tor B
	TCC1.CTRLA =  0x03;								// P�edd�li�ka (N)
	TCC1.CTRLB =  0xf3;								// povol� porovn�v�n� kompar�tor� A,B,C,D v re�imu single slope pwm
	//TCC1.INTCTRLA =  0x01;						// povolen� p�eru�en� p�i p�ete�en�
}

void clk_init()										// fce ��t�n� po�tu uplynul�ch us od zapnut� Xmegy
{
	TCE1.CTRLB = TC_WGMODE_SS_gc;					// re�im timer/counter0 -> single slope mode
	TCE1.CTRLA = TC_CLKSEL_DIV64_gc;				// p�edd�li�ka N = 64
	TCE1.PER   = 65535;								// p�ete�en� p�i na��t�n� 2 * 65535 = 131,072ms
	//TCE1.INTCTRLA = TC_OVFINTLVL_HI_gc;			// vysok� �rove� vyvol�n� p�eru�en� (net�eba - viz ��dek n�)
	EVSYS.CH0MUX = EVSYS_CHMUX_TCE1_OVF_gc;			// naroubov�n� p�eru�en� TCE1 na Event system (pro� vyt�ovat CPU, kdy� to m��e j�t �ikovn� okolo?!)

	TCD1.CTRLB = TC_WGMODE_NORMAL_gc;				// re�im timer/counter0 -> normal mode
	TCD1.CTRLA = TC_CLKSEL_EVCH0_gc;				// Event channel0
	TCD1.PER   = 65535;								// incr reg. TCD0 p�i ka�d�m p�ete�en� registru TCE. tj. ka�d�ch 131,072ms (max. 65535 * 131,072ms = 143,16 minuty)
	TCD1.CTRLD = TC_EVACT_CAPT_gc | TC0_EVDLY_bm | 0X08;	//TC0_EVSEL0_bm;
}

uint32_t count_us()									// fce vracej�c� vypo�ten� po�et uplynul�ch us od zapnut� Xmegy
{
	uint32_t us = 0;
	
	us = ((uint32_t)TCD1.CNT*131072) + ((uint32_t)TCE1.CNT*2);	// p�epo�et rozeps�n ve fci clk_init()
	
	return(us);
}

void initIO()										// nastaven� vstupn�-v�stupn�ch pin�
{
	// nastaven� I/O pin�
	PORTB.DIRSET  = 0x04;							// Nastaven� vybran�ch pin� portu B jako v�stupn� (log. 1)
	PORTC.DIRSET  = 0x30;							// Nastaven� vybran�ch pin� portu C jako v�stupn� (log. 1)
	PORTA.DIRSET  = 0x21;							// Nastaven� vybran�ch pin� portu A jako v�stupn� (log. 1)
	PORTE.DIRSET  = 0x10;							// Nastaven� vybran�ch pin� portu E jako v�stupn� (log. 1)	||  Test I2C - 51 (LSM9DS1)
	PORTF.DIRSET  = 0x9f;							// v�stupn� piny pro PWM serv/motor� (F0 a� F3)	+ dal�� vybran�  ||  Test I2C - 9F (LSM9DS1)
	PORTC.OUTSET = PIN5_bm;							// nastaven� log. 1 na LED2, ne� za�ne pracovat s PWMkem...ochrana proti zabliknut�
	//PORTD.DIRSET  = 0x80;
	
	PORTC.PIN0CTRL = 0x01;							// Pull Up rezistor SDA ---I2C je stejn� na jin�m portu (E)!!
	PORTC.PIN1CTRL = 0x02;							// Pull Up rezistor SCL
	
	// nastaven� v�choz�ch hodnot vybran�ch I/O pin�
	nap_5V(CLR);									// m�ni� 5V je ve v�choz�m nastaven� vypnut� (neg.) POZN.:(do�asn� zapl�)
	P_S(CLR);										// DPS Q5 - light Power Switch je ve v�choz�m nastaven� vypnut� POZN.:(do�asn� zapl�)
	
	// p�i p�ipojen� Li-Pol by mohlo doj�t k z�kmit�m, nebo k chybn�mu stisknut� tla��tka p�i manipulaci (proto tohle o�et�en�)...ke spu�t�n� je pot�eba del�� stisk tla��tka (nesta�� �uknut� do n�j)
	_delay_ms(10);									// �ek� na ust�len�
	if (PORTF.IN & (1<<6))							// pokud je PF6 = 1 (tzn. je stisknuto zap�nac� tla��tko DPS) - o�et�en� proti samovoln�mu zapnut�
	{
		PWR_ON(SET);								// samoudr�en� DPS v zapnut�m stavu i po pu�t�n� zap�nac�ho tla��tka
		iLED(SET);									// ve v�choz�m stavu indika�n� LED sv�t� (odstran�n� latence mezi zapnut�m DPS a rozsv�cen�m LED, 1Hz TCC0 pro iLED)
	}
	else
	{
		PWR_ON(CLR);								// pokud nen� stisknuto tla��tko - vypnout DPS
	}
	
	
	// nastaven� pro extern� p�eru�en�
	PORTF.PIN6CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na PF6, n�b�n� hrana
	PORTF.INT0MASK = PIN6_bm;						// INT0 na PF6
	PORTF.INTCTRL = PORT_INT0LVL_LO_gc;				// priorita p�eru�en� na PF6 - low
	
	// nastaven� pro extern� p�eru�en�
	PORTB.PIN0CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.PIN1CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.PIN3CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.PIN4CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.PIN5CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.PIN6CTRL = PORT_ISC_RISING_gc;			// nastaven� extern�ho p�eru�en� na, n�b�n� hrana
	PORTB.INT0MASK = PIN0_bm | PIN1_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm;	// INT0 na PB piny - namaskov�no
	PORTB.INTCTRL = PORT_INT0LVL_HI_gc;				// priorita p�eru�en� na - MED

	PMIC.CTRL |= PMIC_HILVLEN_bm;					// povolen� p�eru�en� �rovn� HI
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;					// povolen� p�eru�en� �rovn� MED
	PMIC.CTRL |= PMIC_LOLVLEN_bm;					// povolen� p�eru�en� �rovn� LO
	sei();											// povedn� int
}