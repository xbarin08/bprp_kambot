/*
 * Regulators.h
 */ 


#ifndef REGULATORS_H_
#define REGULATORS_H_

#include "LPS25H.h"								// barometr
#include "LSM9DS1.h"							// acc/gyro/mag
#include "Ultrazvuk.h"							// jeho hlavi�ka

#define		K1				3.5 //2.1			// konstanta zes�len� k1 pro rozv�tven� rota�n� pp regul�tor
#define		K2				0.015 //0.6			// konstanta zes�len� k2 pro rozv�tven� rota�n� pp regul�tor
#define		K				0.025 //1			// konstanta zes�len� k pro P regul�tor osy z
#define		L				0.43				// d�lka ramene nesouc� motor
#define		STRMOST			0.0011				// strmost fce pro p�epo�et s�ly tahu na hodnotu registru

void PP_reg_rot_XY();							// rozv�tven� P regul�tor rota�n�ch pohyb� XY
ISR(USARTC0_RXC_vect);


#endif /* REGULATORS_H_ */