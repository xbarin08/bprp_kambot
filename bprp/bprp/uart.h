/*
 * uart.h
 *
 * Created: 27.11.2016 10:26:00
 *  Author: martin
 */ 


#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

uint8_t rx_buf[8];
uint8_t tx_buf[8];

bool prenos_rx,prvni_rx;

void uart_init();



#endif /* UART_H_ */