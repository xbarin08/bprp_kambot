/*
* motory.h
*
* Created: 11.11.2016 14:56:27
*  Author: martin
*/


#ifndef MOTORY_H_
#define MOTORY_H_

#include <avr/io.h>
#include <stdint-gcc.h>
#include <stdbool.h>

void motory_init();
void motory_enable(bool en);
void motor_l(int16_t rychlost);
void motor_r(int16_t rychlost);


#endif /* MOTORY_H_ */