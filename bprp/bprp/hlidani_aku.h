/*
* hlidani_aku.h
*
* Created: 27.11.2016 9:53:17
*  Author: martin
*/


#ifndef HLIDANI_AKU_H_
#define HLIDANI_AKU_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint-gcc.h>

#include "twi_master_driver.h"

#define INA_ADRESA 0b10001010
#define INA_CONFIG 0b0010011111111111
#define INA_CAL ((0.04096)/((1/8192)(0.004)))

TWI_Master_t twi;

void hlidani_init();

#endif /* HLIDANI_AKU_H_ */