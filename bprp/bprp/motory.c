/*
* motory.c
*
* Created: 11.11.2016 14:56:18
*  Author: martin
*/
#include <math.h>
#include <stdlib.h>
#include "motory.h"

void motory_init()
{
	PORTD.DIRSET = PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm;
	PORTD.OUTCLR = PIN6_bm;

	TCD0.CTRLA = TC_TC0_CLKSEL_OFF_gc;
	TCD0.CTRLB = TC0_CCCEN_bm | TC_TC0_WGMODE_FRQ_gc;

	TCD1.CTRLA = TC_TC0_CLKSEL_OFF_gc;
	TCD1.CTRLB = TC0_CCAEN_bm | TC_TC0_WGMODE_FRQ_gc;
}

void motory_enable(bool en)
{
	if(en)
	{
		PORTD.OUTSET = PIN6_bm;
	}
	else
	{
		PORTD.OUTCLR = PIN6_bm;
	}
}

void motor_l(int16_t rychlost)
{
	if (rychlost==0)
	{
		TCD0.CTRLA = TC_TC0_CLKSEL_OFF_gc;
	}
	else
	{
		TCD0.CTRLA = TC_TC0_CLKSEL_DIV256_gc;
		if (rychlost<0)
		{
			PORTD.OUTSET = PIN1_bm;
		}
		else
		{
			PORTD.OUTCLR = PIN1_bm;
		}
		TCD0.CCABUF = abs(rychlost);
	}
}

void motor_r(int16_t rychlost)
{
	if (rychlost==0)
	{
		TCD1.CTRLA = TC_TC0_CLKSEL_OFF_gc;
	}
	else
	{
		TCD1.CTRLA = TC_TC0_CLKSEL_DIV256_gc;
		if (rychlost<0)
		{
			PORTD.OUTCLR = PIN3_bm;
		}
		else
		{
			PORTD.OUTSET = PIN3_bm;
		}
		TCD1.CCABUF = abs(rychlost);
	}
}