/*
* uart.c
*
* Created: 27.11.2016 10:25:49
*  Author: martin
*/

#include "uart.h"
#include "motory.h"

void rx_reset()
{
	DMA.CH0.CTRLA &= ~(DMA_CH_ENABLE_bm);
	DMA.CH0.REPCNT = 0;
	DMA.CH0.CTRLA =  DMA_CH_SINGLE_bm | DMA_CH_REPEAT_bm | DMA_CH_BURSTLEN_1BYTE_gc;
	DMA.CH0.CTRLB = DMA_CH_TRNINTLVL_LO_gc;
	DMA.CH0.TRIGSRC = DMA_CH_TRIGSRC_USARTC1_RXC_gc;
	DMA.CH0.ADDRCTRL = DMA_CH_SRCDIR_FIXED_gc | DMA_CH_DESTRELOAD_BLOCK_gc| DMA_CH_DESTDIR_INC_gc;
	DMA.CH0.SRCADDR0 = ((uint16_t)&USARTC1.DATA) & 0xff;
	DMA.CH0.SRCADDR1 = (uint16_t)&USARTC1.DATA >> 8;
	DMA.CH0.SRCADDR0 = (((uint32_t)&USARTC1.DATA) >> 16);
	DMA.CH0.DESTADDR0 = (((uint16_t) & rx_buf));
	DMA.CH0.DESTADDR1 = (((uint16_t) & rx_buf >> 8));
	DMA.CH0.DESTADDR2 = (((uint32_t) & rx_buf) >> 16);
	DMA.CH0.TRFCNT = 8;
	DMA.CH0.CTRLA |= DMA_CH_ENABLE_bm;
	DMA.CH0.CTRLB |= DMA_CH_ERRIF_bm;
}

void uart_init()
{
	PORTC.DIRSET = PIN7_bm;
	USARTC1.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc;
	USARTC1.BAUDCTRLA = 75;
	USARTC1.BAUDCTRLB = ((-6) << USART_BSCALE_gp);
	USARTC1.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
	USARTC1.CTRLA = USART_RXCINTLVL_LO_gc;

	rx_reset();
	
	DMA.CH1.REPCNT = 0;
	DMA.CH1.CTRLA =  DMA_CH_SINGLE_bm /*| DMA_CH_REPEAT_bm*/ | DMA_CH_BURSTLEN_1BYTE_gc;
	//DMA.CH1.CTRLB = DMA_CH_TRNINTLVL_LO_gc;
	DMA.CH1.TRIGSRC = DMA_CH_TRIGSRC_USARTC1_DRE_gc;
	DMA.CH1.ADDRCTRL = DMA_CH_SRCDIR_INC_gc | DMA_CH_SRCRELOAD_BLOCK_gc | DMA_CH_DESTDIR_FIXED_gc;
	DMA.CH1.DESTADDR0 = ((uint16_t)&USARTC1.DATA) & 0xff;
	DMA.CH1.DESTADDR1= (uint16_t)&USARTC1.DATA >> 8;
	DMA.CH1.DESTADDR2= (((uint32_t)&USARTC1.DATA) >> 16);
	DMA.CH1.SRCADDR0 = (((uint16_t) & tx_buf));
	DMA.CH1.SRCADDR1 = (((uint16_t) & tx_buf >> 8));
	DMA.CH1.SRCADDR2 = (((uint32_t) & tx_buf) >> 16);
	DMA.CH1.TRFCNT = 8;
	DMA.CH1.CTRLA |= DMA_CH_ENABLE_bm;
	
	DMA.CTRL = DMA_ENABLE_bm;

	TCC1.CTRLA = TC_TC1_CLKSEL_OFF_gc;
	TCC1.CTRLB = TC_TC0_WGMODE_NORMAL_gc;
	TCC1.INTCTRLA = TC_TC0_OVFINTLVL_LO_gc;
	TCC1.PER=4096;
	TCC1.CNT=0;

	prenos_rx=false;
	prvni_rx=true;
	motory_init();
}

ISR(USARTC1_RXC_vect)
{
	if(prvni_rx)
	{
		prenos_rx=true;
		prvni_rx=false;
		TCC1.CTRLA = TC_TC1_CLKSEL_DIV1_gc;
		TCC1.CNT=0;
	}
}

ISR(TCC1_OVF_vect)
{
	prenos_rx=false;
	prvni_rx=true;
	TCC1.CTRLA = TC_TC1_CLKSEL_OFF_gc;
	rx_reset();
	uint8_t tmp = USARTC1.DATA;
	memset(rx_buf,0,sizeof(rx_buf));
}

ISR(DMA_CH0_vect)
{
	if(DMA.CH0.CTRLB & DMA_CH_TRNIF_bm)
	{
		if((rx_buf[0] == 0b10101010) && (rx_buf[7] == 0b10101010))
		{
			motory_enable(rx_buf[3]);
			motor_r(rx_buf[1]);
			motor_l(rx_buf[2]);
		}
		TCC1.CTRLA = TC_TC1_CLKSEL_OFF_gc;
		DMA.CH0.CTRLB |= DMA_CH_TRNIF_bm;
		memset(rx_buf,0,sizeof(rx_buf));
		prenos_rx=false;
		prvni_rx=true;
	}
}