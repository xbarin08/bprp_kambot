#ifndef PATH_H
#define PATH_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/core/affine.hpp>
#include "scanline.h"

#define EDGES

class path_t
{
public:
    path_t(const cv::Mat &in) : image(in) {}

    typedef cv::Vec<double,2> Vec2;
    typedef cv::Vec<double,3> Vec3;

    void pathfinding();

    static Vec2 aproximate_line(const std::vector<cv::Point> &path);

    static Vec3 aproximate_quadratic(const std::vector<cv::Point> &path);

    static cv::Point transpose_point_inverse(const cv::Point &in);

    struct path_debug
    {
        std::vector<cv::Point> nodes;
        std::vector<scanline> scanlines;
        std::vector<std::vector<cv::Point>> lines;
        std::vector<Vec2> lineCoef;
        std::vector<Vec3> quadCoef;
    };

    const path_debug &getDebug() const;

private:

    void transpose_path(const std::vector<cv::Point> &path, std::vector<cv::Point> &newpath);
    cv::Point transpose_point(const cv::Point &in);
    void detect_image();
    void create_path();
    void create_path_internal(std::vector<cv::Point> line, std::vector<cv::Point>::iterator it, std::pair<int,int> interval, int depth);
    double length(const cv::Point p1, const cv::Point p2);
    double length_y(const cv::Point p1, const cv::Point p2);
    double length_x(const cv::Point p1, const cv::Point p2);

    path_debug debug;

    cv::Mat image;
};

#endif //PATH_H
