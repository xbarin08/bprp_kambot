#include "scalartorgb.h"
#include <opencv2/imgproc/imgproc.hpp>

ScalarToRGB::ScalarToRGB(double scalarmin, double scalarmax)
{
    k = 120.0f / (scalarmax - scalarmin);
    q = -k * scalarmin;
}

cv::Scalar ScalarToRGB::convert(double scalar)
{
    double h = k * scalar + q;
    return hsv2rgb(h,1,1);
}

cv::Scalar ScalarToRGB::hsv2rgb(double h, double s, double v)
{
    double r,g,b;

    h /= 60.0f;
    int i = static_cast<int>(h);
    double f = h - i;
    double p = v * (1.0f - s);
    double q = v * (1.0f - (s * f));
    double t = v * (1.0f - (s * (1.0f - f)));
    switch (i)
    {
    case 0:
        r = v;
        g = t;
        b = p;
        break;
    case 1:
        r = q;
        g = v;
        b = p;
        break;
    case 2:
        r = p;
        g = v;
        b = t;
        break;
    case 3:
        r = p;
        g = q;
        b = v;
        break;
    case 4:
        r = t;
        g = p;
        b = v;
        break;
    case 5:
        r = v;
        g = p;
        b = q;
        break;
    default:
        r = 0;
        g = 0;
        b = 0;
        break;
    }
    return CV_RGB(255*r,255*g,255*b);
}
