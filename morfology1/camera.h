#ifndef CAMERA_H
#define CAMERA_H

#ifndef DESKTOP_BUILD
#include <raspicam/raspicam_cv.h>
#else
#include <opencv2/highgui/highgui.hpp>
#endif

#include <thread>
#include <mutex>

#define IM_WIDTH 320
#define IM_HEIGHT 240

class camera
{
public:
    camera();
    ~camera();
    
    void open();
    void close();
    
    cv::Mat grab();
    
private:
	
	bool isOpen=false;
    std::mutex acces;
    std::mutex sync;
    std::thread camera_thread;
    volatile bool run=true;
    volatile bool ready=false;
    cv::Mat img;
    static void grab_thread(camera *obj);
    
#ifdef DESKTOP_BUILD
    cv::VideoCapture Camera;
#else
    raspicam::RaspiCam_Cv Camera;
#endif

    
};

#endif // CAMERA_H
