#include "eqhist.h"
#include "path.h"

void eqHist::eq(const cv::Mat &in, cv::Mat &out)
{
    double min=0;
    double max=0;
    minMaxLoc(in,&min,&max,nullptr,nullptr);
    uchar range = max-min;
    center = min+range/2;
#ifndef EDGES
    if(range<50)
    {
        min=center-25;
        max=center+25;
        clamp=true;
    }
#endif
    in.convertTo(out, CV_8U, 255.0/(max - min), -min * 255.0/(max - min));
}

bool eqHist::is_clamped()
{
    return clamp;
}

uchar eqHist::get_center()
{
    return center;
}
