#include "timing_t.h"
#include <iostream>

timing_t::timing_t()
{

}

void timing_t::frame_start()
{
    previous_mark = start_time = cv::getTickCount();
}

void timing_t::time_mark(std::string s)
{
    int64 tmp = cv::getTickCount();
    std::cout << s << " " << double(tmp-previous_mark)/cv::getTickFrequency() << "\n";
    previous_mark = tmp;
}

void timing_t::frame_end(std::string s)
{
    int64 tmp = cv::getTickCount();
    std::cout << s << " " << 1/(double(tmp-start_time)/cv::getTickFrequency()) << "\n";
    start_time = tmp;
}

int64_t timing_t::start_time;
int64_t timing_t::previous_mark;
