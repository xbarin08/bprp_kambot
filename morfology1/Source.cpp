#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <signal.h>
#include <algorithm>
#include "path.h"
#include "telemetry.h"
#include "camera.h"
#include "imagefiltering.h"
#include "timing_t.h"

#ifndef DESKTOP_BUILD
#include "uart.h"
uart_t uart;
#endif

//#define STATIC

camera cap;
timing_t timing;
telemetry_t telemetry;

void clean()
{
#ifdef SHOW
    cvDestroyAllWindows();
#endif
#ifndef STATIC
    cap.close();
#endif
#ifndef DESKTOP_BUILD
    uart.stop();
#endif
}

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        std::cerr << "received SIGINT\n" << std::endl;
    }
    if (signo == SIGFPE)
    {
        std::cerr << "received SIGFPE\n" << std::endl;
    }
    if (signo == SIGABRT)
    {
        std::cerr << "received SIGABRT\n" << std::endl;
    }
    if (signo == SIGSEGV)
    {
        std::cerr << "received SIGSEGV\n" << std::endl;
    }
    clean();
    exit(1);
}

int main(int argc, char* argv[])
{
    try
    {
        if (signal(SIGINT, sig_handler) == SIG_ERR)
        {
            throw std::runtime_error("\ncan't catch SIGINT\n");
        }
        if (signal(SIGFPE, sig_handler) == SIG_ERR)
        {
            throw std::runtime_error("\ncan't catch SIGFPE\n");
        }
        if (signal(SIGABRT, sig_handler) == SIG_ERR)
        {
            throw std::runtime_error("\ncan't catch SIGABRT\n");
        }
        if (signal(SIGSEGV, sig_handler) == SIG_ERR)
        {
            throw std::runtime_error("\ncan't catch SIGSEGV\n");
        }
        
#ifndef DESKTOP_BUILD
        uart.init(B921600,0,1);
        uart.start();
#endif

#ifndef STATIC
        cap.open();
        while(true)
#endif
        {
#if defined(FRAMETIME) || defined(TIMING)
            timing.frame_start();
#endif
#ifdef STATIC
            imageFiltering filter(cv::imread("../tests/03.png",CV_LOAD_IMAGE_GRAYSCALE));
#else
            imageFiltering filter(cap.grab());
#ifdef TIMING
            timing.time_mark("grab");
#endif
#endif
            filter.filter();
            
            path_t pathfinding(filter.getOut());
            pathfinding.pathfinding();
#ifdef TIMING
            timing.time_mark("pathfinding");
#endif
            auto tmp = std::min_element(pathfinding.getDebug().quadCoef.begin(),
                                        pathfinding.getDebug().quadCoef.end(),
                                        [](const path_t::Vec3 &d1, const path_t::Vec3 &d2) -> bool {return std::abs(d1[1]) < std::abs(d2[1]);});
            float atg = 0;


            if(tmp != pathfinding.getDebug().quadCoef.end())
            {
                auto n=*tmp;
                atg = atan(n[1]);
            }


            float proc = (1-fabs(atg*2/M_PI));
            int sign = ((0 < atg) - (atg < 0));
#ifndef DESKTOP_BUILD
            std::cout << "vystup " << proc*sign << "\n";
            uart.motory(proc,sign);
#else
            std::cout << "vystup " << proc*sign << "\n";
#endif      

#if defined(SHOW) || defined(CAPTURE)
            telemetry.createImage(pathfinding.getDebug(),filter.getRoiim(),filter.getEqim(),filter.getSmoothim(),filter.getOut());
#endif
#if defined(FRAMETIME) || defined(TIMING)
            timing.frame_end("snimek");
            std::cout << std::endl;
#endif
#ifdef SHOW
            telemetry.show();
#ifdef STATIC
            cvWaitKey(0);
#else
            char c = cvWaitKey(1);
            if(c==27) //esc
            {
                break;
            }
#endif
#endif
        }
        clean();
    }
    catch(std::runtime_error e)
    {
        std::cerr << e.what() << std::endl;
        clean();
    }

    return 0;
}
