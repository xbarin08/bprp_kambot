#ifndef TELEMETRY_H
#define TELEMETRY_H

#include <opencv2/core/core.hpp>
#include <queue>
#include <thread>
#include <mutex>
#include <vector>
#include "scanline.h"
#include "path.h"

#define SHOW
//#define CAPTURE

class telemetry_t
{
public:
    telemetry_t();
    ~telemetry_t();

    void createImage(const path_t::path_debug &debug, const cv::Mat &roiim, const cv::Mat &eqim, const cv::Mat &smoothim, const cv::Mat &outim);

    void write();
    void show();

private:

    void plot_quadratic(cv::Mat &im, const path_t::Vec3 &coef, cv::Scalar &color);

    std::queue<cv::Mat> buffer;
    std::string prefix;
    std::thread writer;
    static void write_thread(telemetry_t *obj);
    bool run=true;
    std::mutex mtx;
    cv::Mat concat(const std::vector<cv::Mat> &imgs);
    cv::Mat make_color(cv::Mat im);
    cv::Mat join_images_updown(cv::Mat &im_bottom, const cv::Mat &im_top);
    cv::Mat image;
};

#endif
