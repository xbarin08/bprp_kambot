#include "imagefiltering.h"
#include "telemetry.h"
#include <thread>
#include <opencv2/imgproc/imgproc.hpp>

#define ROI_X 0.025
#define ROI_Y 0.025
#define ROI_WIDTH 0.95
#define ROI_HEIGHT 0.95
//definujeme parametry ROI - oblasti zobrazeni, ktera nas zajima



void imageFiltering::filter()
{
    roiim=roi(image);
    eqim=eqhist(roiim);
#if 0
    smoothim=gauss(eqim);
#else
    smoothim=median(eqim);
#endif
#ifdef EDGES
#if 0
    sobel2=sobel2d(smoothim);
    out = sobel2;
#else
    sobel4 = sobel4d(smoothim);
    out = sobel4;
#endif
#else
    bin = threshold(smoothim);
    out = bin;
#endif
}

cv::Mat imageFiltering::gauss(cv::Mat in)
{
    cv::Mat out;
    GaussianBlur(in, out, cv::Size(17, 17), 1.5, 1.5);
#ifdef TIMING
    timing.time_mark("gauss");
#endif
    return out;
}

cv::Mat imageFiltering::bilateral(cv::Mat in)
{
    cv::Mat out;
    cv::bilateralFilter(in,out,5,500,500);
#ifdef TIMING
    timing.time_mark("bilateral");
#endif
    return out;
}

cv::Mat imageFiltering::median(cv::Mat in)
{
    cv::Mat out;
    cv::medianBlur(in,out,3);
#ifdef TIMING
    timing.time_mark("median");
#endif
    return out;
}

cv::Mat imageFiltering::sobel2d(cv::Mat in)
{
    cv::Mat out,tmp1,tmp2;
    in.convertTo(in,CV_16S,32767.0/255.0,-32768);
    Sobel(in,tmp1,CV_16S,1,0);
    Sobel(in,tmp2,CV_16S,0,1);
    out = tmp1 + tmp2;
    out.convertTo(out,CV_8U,255.0/32767.0,127);
#ifdef TIMING
    timing.time_mark("sobel2d");
#endif
    return out;
}

cv::Mat imageFiltering::sobel4d(cv::Mat in)
{
    cv::Mat tmp[3],out;
    in.convertTo(in,CV_16S,32767.0/255.0,-32768);
    
    std::thread thds[3];
    thds[0] = std::thread(imageFiltering::filter_thread,in,&tmp[0],VR);
    thds[1] = std::thread(imageFiltering::filter_thread,in,&tmp[1],MDU);
    thds[2] = std::thread(imageFiltering::filter_thread,in,&tmp[2],mDD);

    for(int i=0;i<3;++i)
    {
        thds[i].join();
    }

    /*cv::imshow("tmp1",tmp[0]);
    cv::imshow("tmp2",tmp[1]);
    cv::imshow("tmp3",tmp[2]);*/

    out = tmp[0] + tmp[1] + tmp[2];

    out.convertTo(out,CV_8U,255.0/32767.0,127);
#ifdef TIMING
    timing.time_mark("sobel4d");
#endif
    return out;
}

cv::Mat imageFiltering::eqhist(cv::Mat in)
{
    cv::Mat out;
    eq.eq(in,out);
#ifdef TIMING
    timing.time_mark("eqhist");
#endif
    return out;
}

cv::Mat imageFiltering::threshold(cv::Mat in)
{
    cv::Mat out;
#if 1
    cv::threshold(in,out,eq.get_center(),255,cv::THRESH_BINARY);
#else
    cv::threshold(in,out,eq.get_center(),255,cv::THRESH_OTSU);
#endif
#ifdef TIMING
    timing.time_mark("threshold");
#endif
    return out;
}

cv::Mat imageFiltering::roi(cv::Mat in)
{
    return in(cv::Rect(ROI_X*image.cols,ROI_Y*image.rows,ROI_WIDTH*image.cols,ROI_HEIGHT*image.rows)); //na jedne strane vznika tmavy artefakt
}

void imageFiltering::filter_thread(const cv::Mat &in, cv::Mat *out, const cv::Mat &kernel)
{
    cv::filter2D(in,*out,-1,kernel);
}

cv::Mat imageFiltering::getSmoothim() const
{
    return smoothim;
}

cv::Mat imageFiltering::getBin() const
{
    return bin;
}

cv::Mat imageFiltering::getEqim() const
{
    return eqim;
}

cv::Mat imageFiltering::getOut() const
{
    return out;
}

cv::Mat imageFiltering::getRoiim() const
{
    return roiim;
}

eqHist imageFiltering::getEq() const
{
    return eq;
}

cv::Mat imageFiltering::getSobel2() const
{
    return sobel2;
}

cv::Mat imageFiltering::getImage() const
{
    return image;
}
