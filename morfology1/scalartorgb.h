#ifndef SCALARTORGB_H
#define SCALARTORGB_H

/**
 * @file scalartorgb.h
 * @brief ScalarToRGB class header
 * @author Martin Bařinka marun1@email.cz
 */

#include <opencv2/core/core.hpp>

/**
 * @brief The ScalarToRGB class implements generating color scale
 * @details this class converts range of values into red-yellow-green color scale.
 */
class ScalarToRGB
{
public:
    /**
     * @brief ScalarToRGB c'tor
     * @param scalarmin minimal input value
     * @param scalarmax maximal input value
     * @details these input values sets conversion scale
     */
    ScalarToRGB(double scalarmin, double scalarmax);

    /**
     * @brief converts scalar into RGB
     * @param scalar input value
     * @return RGB color
     */
    cv::Scalar convert(double scalar);


private:
    cv::Scalar hsv2rgb(double h, double s, double v);

    double k,q;
};

#endif // SCALARTORGB_H
