#include "path.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Dense>
#include "camera.h"

//#define POKUS

// 1 - 254
#ifdef EDGES
#define HORNI_PRAH 223
#define DOLNI_PRAH 32
#else
#define HORNI_PRAH 254
#define DOLNI_PRAH 250
#endif

void path_t::pathfinding()
{
#ifndef EDGES
    if(filter.getEq().is_clamped())
    {
        p1 = p0;
        a=0;
        return;
    }
#endif

    detect_image();

    create_path();

}

void path_t::detect_image()
{
    debug.scanlines.clear();

    unsigned krok_x = image.rows/19; //budeme hledat cestu v kazdem patem radku

    enum class flag { zacatek, nabezna_hrana, sestupna_hrana, konec }; //flag urcuje jestli hledame zacatek (tzn levy okraj) cesty nebo konec (pravy okraj)

    for (int i = image.rows-1; i>=0; i=i-krok_x) //prochazime sloupcemi matici s urcitym krokem
    {
        int Xb=0; //promenna pro stred intervalu (zacatek - konec): zlute body

        scanline line;
        line.line.first=cv::Point(0,i);
        line.line.second=cv::Point(image.cols,i);

        flag hledani=flag::zacatek; //nejdriv budeme hledat zacatek
        scanline::fragment frag;
        for (int j = 0; j<image.cols; ++j) //prochazime radkem
        {
            switch (hledani)
            {
            case flag::zacatek:
                if(image.at<uchar>(i,j)<DOLNI_PRAH)
                {
                    Xb = j;
                    frag.start = cv::Point(j,i);
                    hledani = flag::sestupna_hrana;
                }
                else if(image.at<uchar>(i,j)>HORNI_PRAH)
                {
                    frag.start = cv::Point(0,i);
                    frag.end = cv::Point(j,i);
#ifdef POKUS
                    frag.center = frag.end;
#else
                    frag.center.y = i;
                    frag.center.x = (frag.start.x + frag.end.x)/2;
#endif
                    debug.nodes.push_back(frag.center);
                    line.fragments.push_back(frag);
                    hledani = flag::nabezna_hrana;
                }

                break;
            case flag::sestupna_hrana:
#ifdef POKUS
                if(input.at<uchar>(i,j)>HORNI_PRAH)
#else
                if((image.at<uchar>(i,j)>HORNI_PRAH) || (j==image.cols-1))
#endif
                {
                    frag.end = cv::Point(j,i);
                    Xb = (Xb + j) / 2;
                    frag.center = cv::Point(Xb,i);
                    debug.nodes.push_back(frag.center);
                    line.fragments.push_back(frag);
                    hledani = flag::nabezna_hrana;
                }
#ifdef POKUS
                else if(j==input.cols-1)
                {
                    frag.end = cv::Point(j,i);
                    Xb = (Xb + j) / 2;
                    frag.center = frag.start;
                    nodes.push_back(frag.center);
                    line.fragments.push_back(frag);
                    hledani = flag::konec;
                }
#endif
                break;
            case flag::nabezna_hrana:
                if(image.at<uchar>(i,j)<DOLNI_PRAH)
                {
                    Xb = j;
                    frag.start = cv::Point(j,i);
                    hledani = flag::sestupna_hrana;
                }
                break;
            default:
                break;
            }
        }
        debug.scanlines.push_back(line);
    }
}

void path_t::create_path()
{
    debug.lines.clear();
    std::vector<cv::Point> line;

    if(debug.nodes.size()>1)
    {
        create_path_internal(line,debug.nodes.begin(),std::pair<int,int>(0,image.cols),0);
    }
}

void path_t::create_path_internal(std::vector<cv::Point> line, std::vector<cv::Point>::iterator it, std::pair<int,int> interval, int depth)
{
    if(depth>40)
    {
        debug.lines.push_back(line);
        return;
    }
    int tmp_y = IM_HEIGHT;
    int num_points = 0;
    auto last_i=it;
    for(auto i=it;i!=debug.nodes.end();++i)
    {
        if((i->x<interval.first) || (i->x>interval.second))
        {
            continue;
        }
        /*if(length_x(*i,*last_i)>50)
        {
            continue;
        }*/
        tmp_y=i->y;
        num_points=0;
        last_i=i;
        auto j=i;
        auto last_j=i;
        for(;j!=debug.nodes.end();++j)
        {
            if(((j->x<interval.first) || (j->x>interval.second)))
            {
                continue;
            }
            /*if(length(*j,*last_j)>100)
            {
                continue;
            }*/
            if(j->y==tmp_y)
            {
                ++num_points;
                last_j=j;
            }
            else
            {
                break;
            }
        }
        if(num_points==2)
        {
            int tmp = (i->x+(last_j)->x)/2;
            create_path_internal(line,i,std::pair<int,int>(interval.first,tmp),depth+1);
            create_path_internal(line,i,std::pair<int,int>(tmp,interval.second),depth+1);
            break;
        }
        else if(num_points<2)
        {
            line.push_back(transpose_point(*i));
        }
        if(j==debug.nodes.end())
        {
            debug.lines.push_back(line);
            debug.lineCoef.push_back(aproximate_line(line));
            debug.quadCoef.push_back(aproximate_quadratic(line));
            break;
        }
    }
}

void path_t::transpose_path(const std::vector<cv::Point> &path, std::vector<cv::Point> &newpath)
{
    newpath.clear();
    for(auto i=path.begin();i!=path.end();++i)
    {
        cv::Point pt;
        pt.y = IM_WIDTH/2-(i->x);
        pt.x = IM_HEIGHT-(i->y);
        newpath.push_back(pt);
    }
}

cv::Point path_t::transpose_point(const cv::Point &in)
{
    return cv::Point(IM_HEIGHT-(in.y),IM_WIDTH/2-(in.x));
}

path_t::Vec2 path_t::aproximate_line(const std::vector<cv::Point> &path)
{
    long size=path.size();
    long sum_x=0;
    long sum_y=0;
    long sum_xy=0;
    long sum_x2=0;
    for(auto i : path)
    {
        sum_x+=i.x;
        sum_y+=i.y;
        sum_xy+=(i.x)*(i.y);
        sum_x2+=(i.x)*(i.x);
    }
    return Vec2(double(sum_x2*sum_y-sum_x*sum_xy)/double(size*sum_x2-sum_x*sum_x),double(size*sum_xy-sum_x*sum_y)/double(size*sum_x2-sum_x*sum_x));
}

path_t::Vec3 path_t::aproximate_quadratic(const std::vector<cv::Point> &path)
{
    Eigen::MatrixX3d A;
    Eigen::VectorXd y;

    A.resize(path.size(),3);
    y.resize(path.size());

    for(unsigned i=0;i<path.size();++i)
    {
        A(i,0)=1;
        A(i,1)=path[i].x;
        A(i,2)=(path[i].x*path[i].x);
        y(i)=path[i].y;
    }
    Eigen::Vector3d x = A.colPivHouseholderQr().solve(y);
    return Vec3(x(0),x(1),x(2));
}

cv::Point path_t::transpose_point_inverse(const cv::Point &in)
{
    return cv::Point(IM_WIDTH/2-in.y,IM_HEIGHT-in.x);
}

double path_t::length(const cv::Point p1, const cv::Point p2)
{
    float len_x = p1.x-p2.x;
    float len_y = p1.y-p2.y;
    return sqrt((len_x*len_x)+(len_y*len_y));
}

double path_t::length_y(const cv::Point p1, const cv::Point p2)
{
    return abs(p1.y-p2.y);
}

double path_t::length_x(const cv::Point p1, const cv::Point p2)
{
    return abs(p1.x-p2.x);
}

const path_t::path_debug& path_t::getDebug() const
{
    return debug;
}
