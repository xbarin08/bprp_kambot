#include "camera.h"
#include <thread>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

camera::camera()
{

}

camera::~camera()
{

}

void camera::open()
{
#ifdef DESKTOP_BUILD
    Camera = cv::VideoCapture(0);
    Camera.set(CV_CAP_PROP_FRAME_WIDTH, IM_WIDTH);
    Camera.set(CV_CAP_PROP_FRAME_HEIGHT, IM_HEIGHT);

    if(!Camera.isOpened())
    {
        throw std::runtime_error("camera open error");
    }
#else
    Camera.set(CV_CAP_PROP_FRAME_WIDTH, IM_WIDTH);
    Camera.set(CV_CAP_PROP_FRAME_HEIGHT, IM_HEIGHT);
    Camera.set(CV_CAP_PROP_FORMAT,CV_8U);
    Camera.set(CV_CAP_PROP_EXPOSURE,-1);

    if(!Camera.open())
    {
        throw std::runtime_error("camera open error");
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));
#endif
    camera_thread = std::thread(grab_thread,this);
    isOpen=true;
}

void camera::close()
{
	if(!isOpen)
	{
		return;
	}
    run = false;
    camera_thread.join();
    Camera.release();
}

cv::Mat camera::grab()
{
    cv::Mat tmp;
    while(!ready)
    {
        std::this_thread::sleep_for(std::chrono::microseconds(1));
    }
    acces.lock();
    tmp = img;
    ready = false;
    acces.unlock();
    assert(!tmp.empty());
    return tmp;
}

void camera::grab_thread(camera *obj)
{
#ifndef NDEBUG
    std::cout << "grab startuje " << std::endl;
#endif
    while(obj->run)
    {
        obj->sync.lock();
        obj->Camera.grab();
        obj->sync.unlock();
        obj->acces.lock();
        obj->Camera.retrieve(obj->img);
#ifdef DESKTOP_BUILD
        cvtColor(obj->img, obj->img, cv::COLOR_BGR2GRAY);
#endif
        obj->ready = true;
        obj->acces.unlock();
    }
#ifndef NDEBUG
    std::cout << "grab umrel" << std::endl;
#endif
}
