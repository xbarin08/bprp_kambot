#include "telemetry.h"
#include <chrono>
#include <sstream>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ctime>
#include <iomanip>
#include "camera.h"
#include "scalartorgb.h"
#include "path.h"

telemetry_t::telemetry_t()
{
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    char time[256] = {0};
    strftime(time,256,"%Y.%m.%d-%H.%M.%S",timeinfo);
    char name[256] = {0};
    sprintf(name,"telemetry-%s",time);
    prefix = name;
    writer = std::thread(telemetry_t::write_thread,this);
}

telemetry_t::~telemetry_t()
{
#ifndef NDEBUG
    std::cout << "writer umrel " << std::endl;
#endif
    run = false;
    writer.join();
}

void telemetry_t::createImage(const path_t::path_debug &debug, const cv::Mat &roiim, const cv::Mat &eqim, const cv::Mat &smoothim, const cv::Mat &outim)
{
    cv::Mat cimg;
    cvtColor(outim,cimg,CV_GRAY2RGB);

    for(auto i=debug.scanlines.begin();i!=debug.scanlines.end();++i)
    {
        line(cimg, i->line.first, i->line.second, CV_RGB(255,0,255),1);
        for(auto j=i->fragments.begin(); j<i->fragments.end();++j)
        {
            circle(cimg, j->start, 3, CV_RGB(0,255,0),0);
            circle(cimg, j->end, 3, CV_RGB(64,64,255),0);
        }
    }

    for(auto i=debug.nodes.begin();i!=debug.nodes.end();++i)
    {
        circle(cimg, *i, 3, CV_RGB(255,128,255), 2); // nakreslime uzly
    }

    ScalarToRGB converter(0,debug.lines.size()-1);
    for(auto i=debug.lines.begin();i!=debug.lines.end();++i)
    {
        cv::Scalar color = converter.convert(i-debug.lines.begin());
        /*path_t::Vec2 line_coef = path_t::aproximate_line(*i);
        int x0=0;
        int y0=coef[1]*x0+coef[0];
        int x1=outim.cols;
        int y1=coef[1]*x1+coef[0];
        line(cimg,path_t::transpose_point_inverse(cv::Point(x0,y0)),path_t::transpose_point_inverse(cv::Point(x1,y1)),color,2);*/

        path_t::Vec3 quad_coef = path_t::aproximate_quadratic(*i);
        plot_quadratic(cimg,quad_coef,color);

        int x0=0;
        int y0=quad_coef[1]*x0+quad_coef[0];
        int x1=outim.cols;
        int y1=quad_coef[1]*x1+quad_coef[0];
        line(cimg,path_t::transpose_point_inverse(cv::Point(x0,y0)),path_t::transpose_point_inverse(cv::Point(x1,y1)),color,2);


        for(auto j=i->begin()+1;((j!=i->end())&&(i->size()>1));++j)
        {
            line(cimg,path_t::transpose_point_inverse(*(j-1)),path_t::transpose_point_inverse(*j),color,1);
        }
    }

    std::vector<cv::Mat> imgs;
    imgs.push_back(roiim);
    imgs.push_back(eqim);
    imgs.push_back(smoothim);
    imgs.push_back(cimg);
    image = concat(imgs);
#ifdef CAPTURE
    write();
#endif

}

void telemetry_t::write()
{
    mtx.lock();
    buffer.push(image);
    mtx.unlock();
}

void telemetry_t::show()
{
    cv::imshow("telemetry",image);
}

void telemetry_t::plot_quadratic(cv::Mat &im, const path_t::Vec3 &coef, cv::Scalar &color)
{
    cv::Point last;
    last.x = 0;
    last.y = coef[0];
    for(int i=1;i<im.cols;++i)
    {
        cv::Point p;
        p.x = i;
        p.y = coef[0] + coef[1]*i + coef[2]*i*i;
        line(im,path_t::transpose_point_inverse(last),path_t::transpose_point_inverse(p),color,2);
        last=p;
    }
}

void telemetry_t::write_thread(telemetry_t *obj)
{
    long frame=0;
#ifndef NDEBUG
    std::cout << "writer startuje " << std::endl;
#endif
    while(obj->run)
    {
        obj->mtx.lock();
        if(!(obj->buffer.empty()))
        {
            std::stringstream s;
            s << obj->prefix << "-" << std::setfill('0') << std::setw(5) << frame++ << ".jpg";
            cv::Mat tmp = obj->buffer.front();
            obj->buffer.pop();
            obj->mtx.unlock();
            cv::imwrite(s.str(),tmp);
        }
        else
        {
            obj->mtx.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
#ifndef NDEBUG
    std::cout << "vlakno umrelo " << std::endl;
#endif
}

cv::Mat telemetry_t::concat(const std::vector<cv::Mat> & imgs)
{
    cv::Mat out;
    for(auto i=imgs.begin();i!=imgs.end();++i)
    {
        out = join_images_updown(out,*i);
    }
    return out;
}

cv::Mat telemetry_t::make_color(cv::Mat im)
{
    if(!(im.type() & CV_8UC3))
        cvtColor(im, im, cv::COLOR_GRAY2RGB);
    return im;
}

cv::Mat telemetry_t::join_images_updown(cv::Mat &im_bottom, const cv::Mat &im_top)
{
    // joins two images of same width
    // they must have same width!
    // adds text to the upper left corner
    cv::Size sz_top = im_top.size();
    if(im_bottom.empty())
        im_bottom = cv::Mat::zeros(2, sz_top.width, CV_8UC3); // if it is the first image
    cv::Size sz_bot = im_bottom.size();
    // whole image
    cv::Mat both(sz_top.height + sz_bot.height, sz_bot.width, CV_8UC3);

    // place for text label

    // individual images
    make_color(im_top).copyTo(both(cv::Rect(0, 0, sz_top.width, sz_top.height)));
    make_color(im_bottom).copyTo(both(cv::Rect(0, sz_top.height, sz_bot.width, sz_bot.height)));

    return both;
}
