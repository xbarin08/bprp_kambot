#ifndef IMAGEFILTERING_H
#define IMAGEFILTERING_H

#include <opencv2/core/core.hpp>
#include "eqhist.h"
#include "timing_t.h"

class imageFiltering
{
public:
    imageFiltering(cv::Mat in) : image(in)
    {
		VR*=0.5;
		MDU*=0.5;
		mDD*=0.5;
	}

    void filter();

    cv::Mat getImage() const;

    cv::Mat getSobel2() const;

    eqHist getEq() const;

    cv::Mat getRoiim() const;

    cv::Mat getGaussim() const;

    cv::Mat getOut() const;

    cv::Mat getEqim() const;

    cv::Mat getBin() const;

    cv::Mat getSmoothim() const;

private:

    cv::Mat gauss(cv::Mat in);
    cv::Mat bilateral(cv::Mat in);
    cv::Mat median(cv::Mat in);
    cv::Mat sobel2d(cv::Mat in);
    cv::Mat sobel4d(cv::Mat in);
    cv::Mat eqhist(cv::Mat in);
    cv::Mat threshold(cv::Mat in);
    cv::Mat roi(cv::Mat in);

    static void filter_thread(const cv::Mat &in, cv::Mat *out, const cv::Mat &kernel);

    //vertikalni zprava
    cv::Mat VR = (cv::Mat_<float>(3,3,CV_32F) <<
              -1,	0,	1,
              -2,	0,	2,
              -1,	0,	1);
    //vertikalni zleva
    cv::Mat VL = (cv::Mat_<float>(3,3,CV_32F) <<
              1,	0,	-1,
              2,	0,	-2,
              1,	0,	-1);
    //horizontalni zdola
    cv::Mat HD = (cv::Mat_<float>(3,3,CV_32F) <<
              -1,	-2,	-1,
              0,	0,	0,
              1,	2,	1);
    //horizontalni zhora
    cv::Mat HU = (cv::Mat_<float>(3,3,CV_32F) <<
              1,	2,	1,
              0,	0,	0,
              -1,	-2,	-1);



    //hlavni diagonala zdola
    cv::Mat MDD = (cv::Mat_<float>(3,3,CV_32F) <<
               0,	-1,	-2,
               1,	0,	-1,
               2,	1,	0);
    //hlavni diagonala zhora
    cv::Mat MDU = (cv::Mat_<float>(3,3,CV_32F) <<
               -0,	1,	2,
               -1,	0,	1,
               -2,	-1,	0);
    //vedlejsi diagonala zdola
    cv::Mat mDD = (cv::Mat_<float>(3,3,CV_32F) <<
               -2,	-1,	0,
               -1,	0,	1,
               0,	1,	2);
    //vedlejsi diagonala zhora
    cv::Mat mDU = (cv::Mat_<float>(3,3,CV_32F) <<
               2,	1,	0,
               1,	0,	-1,
               0,	-1,	-2);

    cv::Mat image;
    cv::Mat roiim;
    cv::Mat smoothim;
    cv::Mat sobel2;
    cv::Mat sobel4;
    cv::Mat out;
    cv::Mat eqim;
    cv::Mat bin;
    eqHist eq;
    timing_t timing;
};

#endif // IMAGEFILTERING_H
