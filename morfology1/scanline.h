#ifndef SCANLINE_H
#define SCANLINE_H

#include <opencv2/core/core.hpp>

struct scanline
{
    std::pair<cv::Point,cv::Point> line;
    struct fragment
    {
        cv::Point start,end,center;
    };
    std::vector<fragment> fragments;
};

#endif // SCANLINE_H
