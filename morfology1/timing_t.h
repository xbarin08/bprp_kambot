#ifndef TIMING_T_H
#define TIMING_T_H

#include <opencv2/core/core.hpp>

#define FRAMETIME
//#define TIMING

class timing_t
{
public:
    timing_t();

    void frame_start();
    void time_mark(std::string s=std::string());
    void frame_end(std::string s=std::string());

private:
    static int64_t start_time;
    static int64_t previous_mark;
};

#endif // TIMING_T_H
