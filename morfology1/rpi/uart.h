#ifndef UART_H
#define UART_H

#include <unistd.h>
#include <termios.h>

#define MESSAGE_LENGTH 10

class uart_t
{
public:
    uart_t();
    ~uart_t()
    {
        close(fd);
    }

    void init(int speed, int parity, int blocking);

    void motory(float vychylka, int smer);

    void start();
    void stop();

private:

	enum ecmd : uint8_t {motory_en=0,motor_left=1,motor_right=2,cara=3,vzdalenost=4};

    int set_interface_attribs (int speed, int parity);

    void set_blocking (int should_block);

    void send(void *data, ecmd cmd);
    
    void motor_r(int16_t in);
    
    void motor_l(int16_t in);
    
    void motory_enable(bool in);

    int fd;

};

#endif
