#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <fcntl.h>
#include <iomanip>
#include "uart.h"


uart_t::uart_t()
{

}

void uart_t::init(int speed, int parity, int blocking)
{
    char portname[] = "/dev/serial0";
    fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        std::stringstream s;
        s << "error " << errno << " opening " << portname << ": " << strerror(errno);
        throw std::runtime_error(s.str());
    }


    if(set_interface_attribs(speed,parity))
    {
        throw std::runtime_error("pruser");
    }
    set_blocking(blocking);
}

void uart_t::motory(float vychylka, int smer)
{
    uint16_t motorr=15, motorl=15;
    if(smer>0)
    {
        motorr*=vychylka;
        motorl/=vychylka;
    }
    else if(smer<0)
    {
        motorr/=vychylka;
        motorl*=vychylka;
    }
    if(motorr<3)
    {
        motorr=3;
    }

    if(motorl<3)
    {
        motorl=3;
    }

	motor_l(motorl);
	motor_r(motorr);

}

void uart_t::start()
{
	motor_l(0);
	motor_r(0);
    motory_enable(true);
}

void uart_t::stop()
{
    motory_enable(false);
}

int uart_t::set_interface_attribs (int speed, int parity)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        printf ("error %d from tcgetattr", errno);
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        printf ("error %d from tcsetattr", errno);
        return -1;
    }
    return 0;
}

void uart_t::set_blocking (int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        std::stringstream s;
        s << "error: " << errno << " from tcgetattr";
        throw std::runtime_error(s.str());
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        std::stringstream s;
        s << "error: " << errno << " from tcgetattr";
        throw std::runtime_error(s.str());
    }
}

void uart_t::send(void *in, ecmd cmd)
{
    uint8_t data[MESSAGE_LENGTH] = {0};
	data[0] = 0b10101010;
	data[1] = cmd;
	data[MESSAGE_LENGTH-1] = 0b01010101;
	int16_t *tmp = (int16_t*)(data+2);
	*tmp = *((int16_t*)in);
	uint16_t sum=0;
	for(unsigned i=1;i<MESSAGE_LENGTH-3;++i)
	{
		sum+=data[i];
	}
	data[MESSAGE_LENGTH-3]=sum >> 8;
	data[MESSAGE_LENGTH-2]=sum & 0xff;
	write(fd, data, MESSAGE_LENGTH);
}

void uart_t::motor_r(int16_t in)
{
	send(&in,ecmd::motor_right);
}
    
void uart_t::motor_l(int16_t in)
{
	send(&in,ecmd::motor_left);
}

void uart_t::motory_enable(bool in)
{
	send(&in,ecmd::motory_en);
}
