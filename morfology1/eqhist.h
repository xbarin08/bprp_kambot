#ifndef EQHIST_H
#define EQHIST_H

#include <opencv2/core/core.hpp>

class eqHist
{
public:

    void eq(const cv::Mat& in, cv::Mat& out);
    bool is_clamped();
    uchar get_center();

private:
    bool clamp=false;
    uchar center;

};

#endif // EQHIST_H
